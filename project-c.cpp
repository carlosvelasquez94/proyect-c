#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h> 
#include <ctype.h>

#define ELEM 50
#define USU 2

void gotoxy(short x, short y)//para el funcionamiento del gotoxy
{
	COORD a;
	a.X = x;
	a.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),a);
};
//----------------------------------------------------------------------------------------------------------------------------------
int num_prov=3;
int num_art= 102;

struct datos_articulos{
	int cod_art;
	char desc[30];
	char rubro[30];
	char precio[30];
	int cod_proveedor;
	int baja_logica;
};
struct datos_proveedor{
	int cod_proveedor;
	char razon[30];
	char tel[30];
	char mail[30];
	char direccion[30];
	int baja_logica;
};
struct usuario {       
char usuario[30];
char contra[30];
int perfil;
}; 
struct datos_articulos dat_art[ELEM]={{100,"dolor de cabeza","farmacia","45",1,1},
								  {101,"alergias","farmacia","100",2,1}},aux;//precarga de datos articulos
struct datos_proveedor dat_pro[ELEM]={{1,"bayer","46018510","carlos_16@live.com.ar","timoteo gordillo 4033",1},
                                  {2,"bisolvon","46018545","ferrodriguez@live.com.ar","gaona 3355",1}},aux_prov[ELEM];//precarga datos proeevedor
struct usuario usu_contra[USU]={{"admin","123",1},
						      {"carlos","321",2}};

//----------------------------------------------------------------------------------------------------------------------------------
int menu(void);
int menu_buscar(void);
int menu_altas(void);
int menu_listas(void);
int menu_modificaciones(void);
int menu_bajas(void);
int baja_articulo(void);
int baja2_articulo(void);
int baja_proveedor(void);
int baja2_proveedor(void);
int buscar_articulo(void);
int buscar_proveedor(void);
int modificar_articulo(void);
int modificar_proveedor(void);
int lista_articulos(void);
int lista_proveedor(void);
int lista_pro_art(void);
int alta_articulos(void);
int alta_proveedor(void);
int usuario_cont(void);

int tempo(void);
int cuadro(void);
int cuadroanimado(void);

int x=20;//coordenada
int y=5;//coordenada
int col=1;//variable color
int da=2;//variable para no pisar precarga de articulos
int dp=2;//variable para no pisar precarga de proveedores
int posa;//posicion articulos
int posp;//posicion proveedores
int pos_usu;//posicion del usuario

int alta_1 (void);
int baja_2 (void);
int modificar_3 (void);
int lista_4 (void);
int buscar_5 (void);
int salir_6 (void);
int menu_alta_1(void);
int menu_baja_2(void);
int menu_modificar_3(void);
int menu_lista_4(void);
int menu_buscar_5(void);
int menu_salir_6(void);

int comun_buscar_5 (void);
int comun_lista_4 (void);
int comun_salir_6 (void);
int comun_menu_lista_4(void);
int comun_menu_buscar_5(void);
int comun_menu_salir_6(void);
int perfil_usu;


int validar_num(char numeroo[30]);
int validar_cadena(char cadena[30]);
int validar_correo(char correo[30]);
//----------------------------------------------------------------------------------------------------------------------------------
main()
{
//	int perfil_usu;
	
	perfil_usu = usuario_cont();
	
	if (perfil_usu == 1)
	{
		menu_alta_1();
	}
	else
	{
		comun_menu_lista_4();
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
int usuario_cont()
{
	char usu[30];
	char pass[30];
	int i=0;
	int x=0;
	for(x=0;x<3;x++)
	{	
		system("cls");
		fflush(stdin);
		// inicializamos
		pass[0]='\0'; 
		usu[0]='\0'; 
		i = 0;
		
		printf("ingrese usuario: ");
		gets(usu);
		
		fflush(stdin);
		printf("ingrese password: ");
		
		while(pass[i]!=13) 
    	{
			//Capturamos car�cter 
        	pass[i]=getch(); 
			//Si es un car�cter v�lido y no se ha sobrepasado el l�mite de 20 caracteres se imprime un asterisco 
        	if(pass[i]>32 && i<20) 
            { 
              putchar('*'); 
              i++; 
              //Si se pulsa la tecla RETROCESO, se retrocede un car�cter, se imprime un espacio para eliminar el asterisco y se vuelve a retroceder para que el siguiente asterisco se coloque a continuaci�n del anterior.  
            }else if(pass[i]==8 && i>0) 
            { 
              putchar(8); 
              putchar(' '); 
              putchar(8); 
              i--;  
            } 
		}
		//fin de cadena
		pass[i]='\0'; 
				
		// recorremos struct de usuarios
		for(y=0;y<USU;y++){
			if ( (strcmp(usu_contra[y].usuario,usu)==0) && (strcmp(usu_contra[y].contra,pass)==0) )
			{	
				return usu_contra[y].perfil; // sale de la funcion y va al MAIN
				pos_usu=i;
			}
		}
		
		
		
				
		if(x==2)
		{
			printf("\n\n\n\n\n\t\t** ACCESO DENEGADO ** \n ");  
			printf("\n\n\n\n\n\t\t** 3 INTENTOS INCORRECTOS, HASTA LUEGO !!!! ** \n "); 
			getch();
			exit(0);
		}else{
			printf("\n\n\n\n\n\t\t** ACCESO DENEGADO ** \n "); 
			getch();
		}
		
	}
			
}
//----------------------------------------------------------------------------------------------------------------------------------
int tempo()
{
	int i;
	int j;
		for(i=0;i<100000000;i++)
	{
		j=j+3;
		if(j<68)
		{
			gotoxy(5+j,2);
		//	printf("�");
			gotoxy(5+j,22);
		//	printf("�");
			//
			gotoxy(5+j,0);
		//	printf("�");
			gotoxy(5+j,24);
		//	printf("�");
			//
			if(j<23)
			{
				gotoxy(8,j-1);
			//	printf("�");
				gotoxy(72,j-1);
			//	printf("�");
				//
				gotoxy(6,j-1);
			//	printf("�");
				gotoxy(74,j-1);
			//	printf("�");
				//
			}
		}
	}
}
//-----------------------------------------------------------------------------------------------------------------------------------
int menu_alta_1()
{
    char cTecla;

    alta_1();
	//printf("\r\nPresione un Tecla ...");

    while(cTecla != 27)
    {
       cTecla = getch();
       if(cTecla == 0)
           cTecla = getch();
       else
           switch(cTecla)
           {
            case 13://ENTER
                 system("cls");
				 menu_altas();				 			 
            	 break;
                      
            case 80:
                 menu_baja_2();
                 //flecha_abajo();
           		 break;
            
            //case 75:
              //   printf("\r\n Presiono Flecha izquierda");
            //break;
            
            //case 77:
             //    printf("\r\n Presiono Flecha derecha");
            //break;
            }
    }
}
int menu_baja_2()
{
    char cTecla;

    baja_2();
	//printf("\r\nPresione un Tecla ...");

    while(cTecla != 27)
    {
       cTecla = getch();
       if(cTecla == 0)
           cTecla = getch();
       else
           switch(cTecla)
           {
            case 13://ENTER
                 system("cls");
				 menu_bajas();
				 //printf("\r\n presione TAB para continuar");
				 
            break;
            
           // case 9:TAB
             //    menu_hola();
				 //printf("\r\n Presiono TAB");
            //break;
            
            case 72:
              		menu_alta_1();
				 //printf("\r\n Presiono Flecha Arriba");
            break;
            
            case 80:
                 menu_modificar_3();
                 //flecha abajo
            break;
            
            //case 75:
              //   printf("\r\n Presiono Flecha izquierda");
            //break;
            
            //case 77:
             //    printf("\r\n Presiono Flecha derecha");
            //break;
            }
    }
}

int menu_modificar_3()
{
    char cTecla;

    modificar_3();
	//printf("\r\nPresione un Tecla ...");

    while(cTecla != 27)
    {
       cTecla = getch();
       if(cTecla == 0)
           cTecla = getch();
       else
           switch(cTecla)
           {
            case 13://ENTER
                 system("cls");
				 menu_modificaciones();
				 //printf("\r\n presione TAB para continuar");
				 
            break;
            
           // case 9:TAB
             //    menu_hola();
				 //printf("\r\n Presiono TAB");
            //break;
            
            case 72:
              		menu_baja_2();
				 //printf("\r\n Presiono Flecha Arriba");
            break;
            
            case 80:
                 menu_lista_4();
                 //flecha abajo
            break;
            
            //case 75:
              //   printf("\r\n Presiono Flecha izquierda");
            //break;
            
            //case 77:
             //    printf("\r\n Presiono Flecha derecha");
            //break;
            }
    }
}

int menu_lista_4()
{
    char cTecla;

    lista_4();
	//printf("\r\nPresione un Tecla ...");

    while(cTecla != 27)
    {
       cTecla = getch();
       if(cTecla == 0)
           cTecla = getch();
       else
           switch(cTecla)
           {
            case 13://ENTER
                 system("cls");
				 menu_listas();
				 //printf("\r\n presione TAB para continuar");
				 
            break;
            
           // case 9:TAB
             //    menu_hola();
				 //printf("\r\n Presiono TAB");
            //break;
            
            case 72:
              		menu_modificar_3();
				 //printf("\r\n Presiono Flecha Arriba");
            break;
            
            case 80:
                 menu_buscar_5();
                 //flecha abajo
            break;
            
            //case 75:
              //   printf("\r\n Presiono Flecha izquierda");
            //break;
            
            //case 77:
             //    printf("\r\n Presiono Flecha derecha");
            //break;
            }
    }
}
int menu_buscar_5()
{
    char cTecla;

    buscar_5();
	//printf("\r\nPresione un Tecla ...");

    while(cTecla != 27)
    {
       cTecla = getch();
       if(cTecla == 0)
           cTecla = getch();
       else
           switch(cTecla)
           {
            case 13://ENTER
                 system("cls");
				 menu_buscar();
				 //printf("\r\n presione TAB para continuar");
				 
            break;
            
           // case 9:TAB
             //    menu_hola();
				 //printf("\r\n Presiono TAB");
            //break;
            
            case 72:
              		menu_lista_4();
				 //printf("\r\n Presiono Flecha Arriba");
            break;
            
            case 80:
                 menu_salir_6();
                 //flecha abajo
            break;
            
            //case 75:
              //   printf("\r\n Presiono Flecha izquierda");
            //break;
            
            //case 77:
             //    printf("\r\n Presiono Flecha derecha");
            //break;
            }
    }
}

int menu_salir_6()
{
    char cTecla;

    salir_6();
	//printf("\r\nPresione un Tecla ...");

    while(cTecla != 27)
    {
       cTecla = getch();
       if(cTecla == 0)
           cTecla = getch();
       else
           switch(cTecla)
           {
            case 13://ENTER
                 system("cls");
				 exit(0);
				 
				 //printf("\r\n presione TAB para continuar");
				 
            break;
            
           // case 9:TAB
             //    menu_hola();
				 //printf("\r\n Presiono TAB");
            //break;
            
            case 72:
              		menu_buscar_5();
				 //printf("\r\n Presiono Flecha Arriba");
            break;
            
           // case 80:
             //    salir_6();
                 //flecha abajo
            //break;
            
            //case 75:
              //   printf("\r\n Presiono Flecha izquierda");
            //break;
            
            //case 77:
             //    printf("\r\n Presiono Flecha derecha");
            //break;
            }
    }
}
//--------------------------------------------------------------------------------------------------
int comun_menu_lista_4()
{
    char cTecla;

    comun_lista_4();
	//printf("\r\nPresione un Tecla ...");

    while(cTecla != 27)
    {
       cTecla = getch();
       if(cTecla == 0)
           cTecla = getch();
       else
           switch(cTecla)
           {
            case 13://ENTER
                 system("cls");
				 menu_listas();
				 //printf("\r\n presione TAB para continuar");
				 
            break;
            
           // case 9:TAB
             //    menu_hola();
				 //printf("\r\n Presiono TAB");
            //break;
            
            //case 72:
              //		menu_modificar_3();
				 //printf("\r\n Presiono Flecha Arriba");
            //break;
            
            case 80:
                 comun_menu_buscar_5();
                 //flecha abajo
            break;
            
            //case 75:
              //   printf("\r\n Presiono Flecha izquierda");
            //break;
            
            //case 77:
             //    printf("\r\n Presiono Flecha derecha");
            //break;
            }
    }
}
int comun_menu_buscar_5()
{
    char cTecla;

    comun_buscar_5();
	//printf("\r\nPresione un Tecla ...");

    while(cTecla != 27)
    {
       cTecla = getch();
       if(cTecla == 0)
           cTecla = getch();
       else
           switch(cTecla)
           {
            case 13://ENTER
                 system("cls");
				 menu_buscar();
				 //printf("\r\n presione TAB para continuar");
				 
            break;
            
           // case 9:TAB
             //    menu_hola();
				 //printf("\r\n Presiono TAB");
            //break;
            
            case 72:
              		comun_menu_lista_4();
				 //printf("\r\n Presiono Flecha Arriba");
            break;
            
            case 80:
                 comun_menu_salir_6();
                 //flecha abajo
            break;
            
            //case 75:
              //   printf("\r\n Presiono Flecha izquierda");
            //break;
            
            //case 77:
             //    printf("\r\n Presiono Flecha derecha");
            //break;
            }
    }
}

int comun_menu_salir_6()
{
    char cTecla;

    comun_salir_6();
	//printf("\r\nPresione un Tecla ...");

    while(cTecla != 27)
    {
       cTecla = getch();
       if(cTecla == 0)
           cTecla = getch();
       else
           switch(cTecla)
           {
            case 13://ENTER
                 system("cls");
				 exit(0);
				 
				 //printf("\r\n presione TAB para continuar");
				 
            break;
            
           // case 9:TAB
             //    menu_hola();
				 //printf("\r\n Presiono TAB");
            //break;
            
            case 72:
              		comun_menu_buscar_5();
				 //printf("\r\n Presiono Flecha Arriba");
            break;
            
           // case 80:
             //    salir_6();
                 //flecha abajo
            //break;
            
            //case 75:
              //   printf("\r\n Presiono Flecha izquierda");
            //break;
            
            //case 77:
             //    printf("\r\n Presiono Flecha derecha");
            //break;
            }
    }
}

int comun_lista_4()
{
	system("cls");
	int i, w=24;
	int x=10,y=5,j;

	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
	j=strlen("lista");
	j++;
	j++;
	for(i=0;i<j;i++)
	{
		
		gotoxy(w,11);
		printf("*");
	 	gotoxy(w,13);
		printf("*");
		w++;
	}
	w=3;
				cuadro();
		/*
		gotoxy(10,4);
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		printf("Alta\n");
		
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(15,6);
		printf("Baja\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(20,9);
		printf("Modificar\n");
	
	//	tempo();
		*/
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(25,12);
		printf("Listas\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(30,15);
		printf("Buscador\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(35,18);
		printf("Salir\n");
}

int comun_buscar_5()
{
	system("cls");
	int i, w=29;
	int x=10,y=5,j;

	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
	j=strlen("buscar");
	j++;
	j++;
	for(i=0;i<j;i++)
	{
		gotoxy(w,14);
		printf("*");
		gotoxy(w,16);
		printf("*");
		w++;
	}
	w=3;
				cuadro();
		
	/*	gotoxy(10,4);
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		printf("Alta\n");
		
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(15,6);
		printf("Baja\n");
		
		//tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(20,9);
		printf("Modificar\n");
	
	//	tempo();
	*/	
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(25,12);
		printf("Listas\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(30,15);
		printf("Buscador\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(35,18);
		printf("Salir\n");
}
int comun_salir_6()
{
	system("cls");
	int i, w=34;
	int x=10,y=5,j;

	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
	j=strlen("salir");
	j++;
	j++;
	for(i=0;i<j;i++)
	{
		gotoxy(w,17);
		printf("*");
		gotoxy(w,19);
		printf("*");
		w++;
	}
	w=3;
		cuadro();
		
		/*gotoxy(10,4);
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		printf("Alta\n");
		
		//tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(15,6);
		printf("Baja\n");
		tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(20,9);
		printf("Modificar\n");
	
	//	tempo();
		*/
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(25,12);
		printf("Listas\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(30,15);
		printf("Buscador\n");

//		tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(35,18);
		printf("Salir\n");
}
//-----------------------------------------------------------------------------------------------------------------------------------
int alta_1()
{
	system("cls");
	int i, w=9;
	int x=10,y=5,j;

	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
	j=strlen("alta");
	j++;
	j++;
	for(i=0;i<j;i++)
	{
		gotoxy(w,3);
		printf("*");
		gotoxy(w,5);
		printf("*");
		w++;
	}
	w=3;
				cuadroanimado();
		gotoxy(10,4);
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		printf("Alta\n");
		
		tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(15,6);
		printf("Baja\n");
		tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(20,9);
		printf("Modificar\n");
		tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(25,12);
		printf("Listas\n");
		tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(30,15);
		printf("Buscador\n");
		tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(35,18);
		printf("Salir\n");
}

int baja_2()
{
	system("cls");
	int i, w=14;
	int x=10,y=5,j;

	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
	j=strlen("baja");
	j++;
	j++;
	for(i=0;i<j;i++)
	{
		gotoxy(w,5);
		printf("*");
		gotoxy(w,7);
		printf("*");
		w++;
	}
	w=3;
				cuadro();
		
		gotoxy(10,4);
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		printf("Alta\n");
		
		//tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(15,6);
		printf("Baja\n");
		
		//tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(20,9);
		printf("Modificar\n");
		
		//tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(25,12);
		printf("Listas\n");
		
		//tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(30,15);
		printf("Buscador\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(35,18);
		printf("Salir\n");
}

int modificar_3()
{
system("cls");
	int i, w=19;
	int x=10,y=5,j;

	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
	j=strlen("modificador");
	j++;
	j++;
	for(i=0;i<j;i++)
	{
		gotoxy(w,8);
		printf("*");
		gotoxy(w,10);
		printf("*");
		w++;
	}
	w=3;
				cuadro();
		
		gotoxy(10,4);
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		printf("Alta\n");
		
		//tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(15,6);
		printf("Baja\n");
		
		//tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(20,9);
		printf("Modificar\n");
		tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(25,12);
		printf("Listas\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(30,15);
		printf("Buscador\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(35,18);
		printf("Salir\n");
}
int lista_4()
{
	system("cls");
	int i, w=24;
	int x=10,y=5,j;

	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
	j=strlen("lista");
	j++;
	j++;
	for(i=0;i<j;i++)
	{
		gotoxy(w,11);
		printf("*");
		gotoxy(w,13);
		printf("*");
		w++;
	}
	w=3;
				cuadro();
		
		gotoxy(10,4);
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		printf("Alta\n");
		
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(15,6);
		printf("Baja\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(20,9);
		printf("Modificar\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(25,12);
		printf("Listas\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(30,15);
		printf("Buscador\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(35,18);
		printf("Salir\n");
}

int buscar_5()
{
	system("cls");
	int i, w=29;
	int x=10,y=5,j;

	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
	j=strlen("buscar");
	j++;
	j++;
	for(i=0;i<j;i++)
	{
		gotoxy(w,14);
		printf("*");
		gotoxy(w,16);
		printf("*");
		w++;
	}
	w=3;
				cuadro();
		
		gotoxy(10,4);
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		printf("Alta\n");
		
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(15,6);
		printf("Baja\n");
		
		//tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(20,9);
		printf("Modificar\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(25,12);
		printf("Listas\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(30,15);
		printf("Buscador\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(35,18);
		printf("Salir\n");
}
int salir_6()
{
	system("cls");
	int i, w=34;
	int x=10,y=5,j;

	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
	j=strlen("salir");
	j++;
	j++;
	for(i=0;i<j;i++)
	{
		gotoxy(w,17);
		printf("*");
		gotoxy(w,19);
		printf("*");
		w++;
	}
	w=3;
		cuadro();
		
		gotoxy(10,4);
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		printf("Alta\n");
		
		//tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(15,6);
		printf("Baja\n");
		tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(20,9);
		printf("Modificar\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(25,12);
		printf("Listas\n");
	
	//	tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(30,15);
		printf("Buscador\n");

//		tempo();
		
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(35,18);
		printf("Salir\n");
}

//-----------------------------------------------------------------------------------------------------------------------------------
/*int menu()
{
	int c=5;
	
	while ((c>4)||(c==0))
	{
		cuadroanimado();
		gotoxy(10,3);
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		printf("1-Alta\n");
		y++;
		tempo();
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(15,6);
		printf("2-Baja\n");
		y++;
		tempo();
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(20,9);
		printf("3-Modificar\n");
		y++;
		tempo();
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(25,12);
		printf("4-Listas\n");
		y++;
		tempo();
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(30,15);
		printf("5-Buscador\n");
		y++;
		tempo();
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
		gotoxy(35,18);
		printf("6-Salir\n");
		y++;
		SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),14);
		gotoxy(40,21);
		scanf("%d",&c);

		y=5;
		fflush(stdin);
		switch(c)
		{
			case 1:
				system("cls");
				y=5;
				menu_altas();
				break;;
			case 2:
				system("cls");
				y=5;
				menu_bajas();
				break;
			case 3:
				system("cls");
				y=5;
				menu_modificaciones();
				break;
			case 4:
				system("cls");
				y=5;
				menu_listas();
				break;
			case 5:
				system("cls");
				y=5;
				menu_buscar();
			case 6:
				system("cls");
				y=5;
				exit(0);
				break;
		    default:
		    	y=y+7;
		    	gotoxy(45,21);
		    	printf("ERROR en el ingreso\n");
		    	y=5;
		    	getch();
		    	system("cls");
				break; 
		}
		
	}
	getch();
}*/

// funcion que valida si existe el cod proveedor
int valid_cod_prov (int cod_prov){
	if ((cod_prov >= 1) && (cod_prov < num_prov))
		return 0;
	else 
		return 1;	
	
}

//--------------------------------------------------------------------------------------------------
int alta_articulos()
{
	int sw;
	int sn;
	int o;
	int x = 20;
	int y = 5;
	int aux_cod_prov;
	int pp=8;
	int i;		
			cuadro();
			//gotoxy(x,y);
			//printf("Ingrese c\xA2""digo de articulo\n");
			//y++;
			//gotoxy(x,y);
			//scanf("%d",&dat_art[da].cod_art);
			//fflush(stdin);
		int d=1;	
		while(d==1)
		{
			system("cls");
			cuadro();
			y=5;
			fflush(stdin);						
			gotoxy(x,y);
			printf("Ingrese descripcion del articulo\n");			
			y++;
			gotoxy(x,y);
			gets(dat_art[da].desc);
			
			sw=validar_cadena(dat_art[da].desc);
			
			if(sw==0)
			{
				gotoxy(58,6);
				printf("dato valido");
				d++;
			}
			else
			{
				gotoxy(57,6);	
				printf("dato invalido");
				gotoxy(55,7);
				printf("ingrese solo letras");
				getch();
			}
		}//cierre del while	
		int r=1;
		while(r==1)	
		{	system("cls");
			cuadro();
			y=5;
			gotoxy(x,y);
			fflush(stdin);
			printf("Ingrese rubro del articulo\n");			
			y++;
			gotoxy(x,y);
			gets(dat_art[da].rubro);
			
			sw=validar_cadena(dat_art[da].rubro);
			
			if(sw==0)
			{
				gotoxy(58,6);
				printf("dato valido");
				r++;
			}
			else
			{
				gotoxy(57,6);	
				printf("dato invalido");
				gotoxy(55,7);
				printf("ingrese solo letras");
				getch();
			}
		}
			y++;
			do {
				system("cls");
				cuadro();
				//principio cartel detalle
				pp=8;
				gotoxy(58,5);
				printf("c\xA2""digo");
				gotoxy(58,6);
				printf("proveedor");
				gotoxy(56,4);
				printf("�������������");
				gotoxy(56,6);
				printf("�");
				gotoxy(68,6);
				printf("�");
				gotoxy(56,5);
				printf("�");
				gotoxy(68,5);
				printf("�");
				gotoxy(56,7);
				printf("�������������");
				for(i=0;i<dp;i++)//cartel indicacion de ventas existente
				{
					if(dat_pro[i].baja_logica==1)
					{
						gotoxy(68,pp);
						printf("�");
						gotoxy(56,pp);
						printf("�");
						gotoxy(58,pp);
						printf("%d",dat_pro[i].cod_proveedor);
						pp++;
					}
				}
				y=5;
				gotoxy(x,y);
				printf("Ingrese c\xA2""digo del proveedor\n");			
				y++;
				gotoxy(x,y);
				scanf("%d",&aux_cod_prov);
				
				if (valid_cod_prov(aux_cod_prov) == 0) {
					dat_art[da].cod_proveedor = aux_cod_prov ;				
					dat_art[da].cod_art = num_art;								
				}
				else{	
					gotoxy(x,y);		
					printf("ERROR: c\xA2""digo de proveedor incorrecto \n");
					getch();
				}				
			}while (valid_cod_prov(aux_cod_prov) != 0);
		int pr=1;			
		while(pr==1)
		{	
			system("cls");
			cuadro();
			y=5;
			gotoxy(x,y);
			fflush(stdin);			
			printf("Ingrese precio del articulo\n");
			y++;
			gotoxy(x,y);
			gets(dat_art[da].precio);
			
			sn=validar_num(dat_art[da].precio);
			
			if(sn==0)
			{
				gotoxy(56,6);
				printf("dato valido");
				pr++;
			}
			else
			{
				gotoxy(56,6);
				printf("dato invalido");
				gotoxy(54,7);
				printf("ingrese solo numeros");
				getch();
			}
		}
			dat_art[da].baja_logica=1;//activo
			da++;
			
			y++;
			gotoxy(x,y);			
			printf("Su c\xA2""digo de articulo es= %d \n",num_art);
			num_art++; // para el prox art nuevo
	

			y++;
			gotoxy(x,y);
			printf("�Desea ingresar otro articulo?1-si, 2-no");		
			y++;
			gotoxy(x,y);
			scanf("%d",&o);
		
			switch (o)
			{
				case 1: 
					system("cls");
					alta_articulos();
					break;
		 		case 2:
					system("cls");
					menu_alta_1();
					break;
				default :
					printf("ERROR en el ingreso\n");	
					getch();
					system("cls");							
			}

			
}//fin alta articulos
//--------------------------------------------------------------------------------------------------
int validar_num(char numeroo[30])
{
	int i=0,sn=0,j;
	
	j=strlen(numeroo);//strlen sirve para contar cuantas letras tienen la cadena
	
	while(i<j && sn==0)
	{
		if(isdigit(numeroo[i])!=0)//isdigit retorna distinto de 0 si es un numero, si es letra reetorna 0
		{
			i++;
		}
		else
		{
			sn=1;
		}
	}
	return(sn);
}

//--------------------------------------------------------------------------------------------------
int validar_cadena(char cadena[30])
{
	int i=0,sw=0,j;
	j=strlen(cadena);//cuenta cuantas letras tiene la cadena
	
	while(i<j && sw==0)
	{
		if(isalpha(cadena[i])!=0)//isalpha si es letra retorna distinto de 0 si es numero retorna 0
			{
				i++;
			}		
		else
		{
			sw=1;
		}
	}
	return(sw);
}
int validar_correo(char correo[30])
{
	int i=0,co=0,j;
		
	j=strlen(correo);
	
	while (i<j && co==0)
	{
		if(correo[i]!='@')
		{
			i++;
		}
		else
		{
			co=1;
		}
	}
	return(co);
}
int validar_telefono(char telefono[30])
{
	int i=0,tte=0,j;
	
	j=strlen(telefono);
	
	
}
//--------------------------------------------------------------------------------------------------
int alta_proveedor()
{
	int o;
	int y=5;
	int x=20;
	int sw;
	int sn;
	int co;	
			
			cuadro();
			dat_pro[dp].cod_proveedor = num_prov;
			gotoxy(x,y);
			printf("Su c\xA2""digo de proveedor es = %d \n",num_prov);
			dat_pro[dp].cod_proveedor=num_prov;
			num_prov++; // para el siguiente proveedor			
			getch();
	int rz=1;		
	while(rz==1)
		{
			system("cls");
			cuadro();
			y=5;
			gotoxy(x,y);
			fflush(stdin);
			printf("Ingrese razon social\n");
			y++;
			gotoxy(x,y);
			gets(dat_pro[dp].razon);
			
			sw=validar_cadena(dat_pro[dp].razon);
			
			if (sw==0)
			{
				gotoxy(58,6);
				printf("dato valido");
				rz++;
				
			}
			else
			{
				gotoxy(57,6);	
				printf("dato invalido");
				gotoxy(55,7);
				printf("ingrese solo letras");
				getch();
			}
		}
	int te=1;
	while(te==1)
		{	
			int su=0;
			int xx;
			system("cls");
			cuadro();
			y=5;
			gotoxy(x,y);
			fflush(stdin);
			printf("Ingrese telefono\n");
			y++;
			gotoxy(x,y);
			gets(dat_pro[dp].tel);
			xx=strlen(dat_pro[dp].tel);
			if(xx==8)
			{		
			sn=validar_num(dat_pro[dp].tel);
				
				if(sn==0)
				{
					gotoxy(56,6);
					printf("dato valido");
					te++;
				}
				else
				{
					gotoxy(56,6);
					printf("dato invalido");
					gotoxy(54,7);
					printf("ingrese solo numeros");
					getch();
				}
				su++;
			}
			if(su==0)
			{
				gotoxy(54,6);
					printf("dato invalido");
					gotoxy(48,7);
					printf("Falta completar los 8 digitos");
					gotoxy(48,8);
					printf("o se exedio de los 8 digitos");
					getch();
			}
		}
	int em=1;
	while(em==1)		
		{
			system("cls");
			cuadro();
			y=5;
			gotoxy(x,y);
			fflush(stdin);	
			printf("Ingrese e-mail\n");
			y++;
			gotoxy(x,y);
			gets(dat_pro[dp].mail);
		
			co=validar_correo(dat_pro[dp].mail);
			
			if(co==1)
			{
				gotoxy(56,6);
				printf("dato valido");
				em++;
			}
			else
			{
				gotoxy(56,6);
				printf("dato invalido");
				gotoxy(54,7);
				printf("Verifique si contiene @ ");
				getch();
				
			}
		}
			y++;
			gotoxy(x,y);
			printf("Ingrese direccion\n");
			y++;
			gotoxy(x,y);
			gets(dat_pro[dp].direccion);
			fflush(stdin);
		
			y++;
			dat_pro[dp].baja_logica=1;//activo
			
			gotoxy(x,y);
			dp++;
			printf("�Desea dar de alta otro proveedor?1-si, 2-no ");
			y++;
			gotoxy(x,y);
			scanf("%d",&o);
			switch (o)
			{
				case 1: 
					system("cls");
					alta_proveedor();
					break;
				case 2:
					system("cls");
					menu_altas();
					break;
				default :
					printf("ERROR en el ingreso\n");	
					getch();
					system("cls");							
			}
			

}//fin alta proveedor
//--------------------------------------------------------------------------------------------------
int	menu_listas()
{
	int op;
	cuadro();
	gotoxy(20,3);
	printf("Menu listas");
	gotoxy(15,5);
	printf("1-lista articulos\n");
	gotoxy(15,6);
	printf("2-lista proveedor\n");
	gotoxy(15,7);
	printf("3-lista proveedor con sus articulos");
	gotoxy(15,8);
	printf("4-Volver");
	gotoxy(15,9);
	scanf("%d",&op);
	switch (op)
	{
		case 1:
			system("cls");
			lista_articulos();
			break;
		case 2:
			system("cls");
			lista_proveedor();
			break;
		case 3:
			system("cls");
			lista_pro_art();
			break;
		case 4:
			system("cls");
			
			if(perfil_usu==1)
			{
				menu_alta_1();
			}
			else
			{
				comun_menu_lista_4();
			}
			break;
		default :
			gotoxy(20,9);
			printf("ERROR en el ingreso\n");	
			getch();
			system("cls");
			menu_listas();			
	}
}
//--------------------------------------------------------------------------------------------------
int menu_modificaciones()
{	
	int op;
	cuadro();
	gotoxy(20,3);
	printf("Menu modificaciones");
	gotoxy(15,5);
	printf("1-Modificar articulos\n");
	gotoxy(15,6);
	printf("2-Modificar Proveedor\n");
	gotoxy(15,7);
	printf("3-Volver\n");
	gotoxy(20,8);
	scanf("%d",&op);
	switch (op)
	{
		case 1:
			system("cls");
			modificar_articulo();
			break;
		case 2:
			system("cls");
			modificar_proveedor();
			break;
		case 3:
			system("cls");
			menu_alta_1();	
		default :
			gotoxy(20,9);
			printf("ERROR en el ingreso\n");	
			getch();
			system("cls");		
	}
	
}//fin menu altas
//--------------------------------------------------------------------------------------------------
int menu_altas()
{	int op;
	cuadro();
	gotoxy(20,3);
	printf("Menu altas");
	gotoxy(15,5);
	printf("1-Altas articulos\n");
	gotoxy(15,6);
	printf("2-Altas proveedor\n");
	gotoxy(15,7);
	printf("3-Volver\n");
	gotoxy(20,8);
	scanf("%d",&op);
	switch (op)
	{
		case 1:
			system("cls");
			alta_articulos();
			break;
		case 2:
			system("cls");
			alta_proveedor();
			break;
		case 3:
			system("cls");
			menu_alta_1();
			break;
		default :
			gotoxy(20,9);
			printf("ERROR en el ingreso\n");	
			getch();
			system("cls");		
	}
	
}//fin menu altas
//--------------------------------------------------------------------------------------------------
int menu_buscar()
{
	int op;
	cuadro();
	gotoxy(20,3);
	printf("Menu Buscardor");
	gotoxy(15,5);
	printf("1-Buscar articulos\n");
	gotoxy(15,6);
	printf("2-Buscar Proveedor\n");
	gotoxy(15,7);
	printf("3-Volver\n");
	gotoxy(15,8);
	scanf("%d",&op);
	switch (op)
	{
		case 1:
			system("cls");
			buscar_articulo();
			break;
		case 2:
			system("cls");
			buscar_proveedor();
			break;
		case 3:
			system("cls");
			if(perfil_usu==1)
			{
				menu_alta_1();
			}
			else
			{
				comun_menu_lista_4();
			}
			break;
		default :
			gotoxy(20,9);
			printf("ERROR en el ingreso\n");	
			getch();
			system("cls");		
	}

}
//--------------------------------------------------------------------------------------------------
int menu_bajas()
{
	int op;
	cuadro();
	gotoxy(20,3);
	printf("Menu bajas");
	gotoxy(15,5);
	printf("1-Baja articulos\n");
	gotoxy(15,6);
	printf("2-Baja proveedor\n");
	gotoxy(15,7);
	printf("3-Volver\n");
	gotoxy(15,8);
	scanf("%d",&op);
	switch (op)
	{
		case 1:
			system("cls");
			baja_articulo();
			break;
		case 2:
			system("cls");
			baja_proveedor();
			break;
		case 3:
			system("cls");
			menu_alta_1();
			break;
		default :
			gotoxy(20,9);
			printf("ERROR en el ingreso\n");	
			getch();
			system("cls");		
	}
}
//--------------------------------------------------------------------------------------------------
int baja_articulo()
{
	int codigo,i,o=1;
	int cont;
				while(o==1)
			{	
				 cont=0;
				y=5;
				system("cls");
				cuadro();
				gotoxy(x,y);
				printf("Ingrese c\xA2""digo\n");
				y++;
				gotoxy(x,y);
				scanf("%d",&codigo);
				for(i=0;i<da;i++)
				{
					if(dat_art[i].baja_logica==1)
					{	
						if(dat_art[i].cod_art==codigo)
						{	
							system("cls");
							cuadro();
							gotoxy(x,y);
							printf("El articulo fue encontrado!");
							posa=i;
							baja2_articulo();
							cont++;
						}
					}
					if(cont==0)
						{
							
							system("cls");
							cuadro();
							gotoxy(x,6);
							printf("c\xA2""digo no encontrado!");
													
						}
				}
				gotoxy(11,13);
				printf("�Desea Buscar otro articulo atravez del c\xA2""digo para dar de baja?");
				gotoxy(x,14);
				printf("1-si");
				gotoxy(x,15);
				printf("2-no");
				gotoxy(x,16);
				scanf("%d",&o);
			}
			system("cls");
			menu_bajas();
}
//--------------------------------------------------------------------------------------------------
int baja2_articulo()
{
	int op=3;
	
	while((op>2)||(op==0))
	{
		cuadro();

		y=5;
		gotoxy(x,y);					
		printf("-----Datos de articulo-----\n");					
		y++;
		gotoxy(x,y);
		printf("c\xA2""digo articulo: %d\n",dat_art[posa].cod_art);
		y++;
		gotoxy(x,y);
		printf("Descripcion: %s\n",dat_art[posa].desc);
		y++;
		gotoxy(x,y);
		printf("Rubro: %s\n",dat_art[posa].rubro);
		y++;
		gotoxy(x,y);
		printf("c\xA2""digo proveedor: %d\n",dat_art[posa].cod_proveedor);
		y++;
		gotoxy(x,y);
		printf("Precio: %d\n",dat_art[posa].precio);
		y++;
		gotoxy(x,y);
		printf("�desea dar de baja este articulo \n?");
		y++;
		gotoxy(x,y);
		printf("1_si\n");
		y++;
		gotoxy(x,y);
		printf("2_no\n");
		y++;
		gotoxy(x,y);
		scanf("%d",&op);
		fflush(stdin);
		y=5;
		system("cls");
		switch(op)
		{
			case 1:
				cuadro();
				dat_art[posa].baja_logica=0;//inactivo
				gotoxy(x,y);
				printf("fue eliminado correctamente\n");
				getch();
				system("cls");
				menu_bajas();
				break;				
			case 2:
				system("cls");
				menu_bajas();
				break;
			default:
				cuadro();
				gotoxy(x,y);
				printf("ERROR en el ingreso\n");
				getch;
				system("cls");
				menu_bajas();
				break;
		}
	}
}
//--------------------------------------------------------------------------------------------------
int baja_proveedor()
{
	int codigo,i,o=1;
	int cont;
	int x=20,y=5;
	int pp=8;	
				cuadro();
				while(o==1)
			{	
				 cont=0;
				y=5;
				system("cls");
				cuadro();
								//principio cartel detalle
				pp=8;
				gotoxy(58,5);
				printf("c\xA2""digo");
				gotoxy(58,6);
				printf("proveedor");
				gotoxy(56,4);
				printf("�������������");
				gotoxy(56,6);
				printf("�");
				gotoxy(68,6);
				printf("�");
				gotoxy(56,5);
				printf("�");
				gotoxy(68,5);
				printf("�");
				gotoxy(56,7);
				printf("�������������");
				for(i=0;i<dp;i++)//cartel indicacion de ventas existente
				{
					if(dat_pro[i].baja_logica==1)
					{
						gotoxy(68,pp);
						printf("�");
						gotoxy(56,pp);
						printf("�");
						gotoxy(58,pp);
						printf("%d",dat_pro[i].cod_proveedor);
						pp++;
					}
				}

				gotoxy(x,y);
				printf("Ingrese c\xA2""digo de proveedor\n");
				y++;
				gotoxy(x,y);
				scanf("%d",&codigo);
				for(i=0;i<dp;i++)
				{
					if(dat_pro[i].baja_logica==1)
					{	
						if(dat_pro[i].cod_proveedor==codigo)
						{	
							system("cls");
							cuadro();
							gotoxy(x,y);
							printf("El proveedro fue encontrado!");
							getch();
							posp=i;
							baja2_proveedor();
							cont++;
						}
					}
					if(cont==0)
						{
							
							system("cls");
							cuadro();
							gotoxy(x,6);
							printf("c\xA2""digo no encontrado!");
													
						}
				}
				gotoxy(10,13);
				printf("�Desea Buscar otro proveedor atravez del c\xA2""digo para dar de baja?");
				gotoxy(x,14);
				printf("1-si");
				gotoxy(x,15);
				printf("2-no");
				gotoxy(x,16);
				scanf("%d",&o);
			}
			system("cls");
			menu_bajas();
}
//--------------------------------------------------------------------------------------------------
int baja2_proveedor()
{
	int op=3;
	int x=20,y=5;
	while((op>2)||(op==0))
	{
		system("cls");
		cuadro();
		
		y=5;
		gotoxy(x,y);					
		printf("c\xA2""digo p:  %d\n",dat_pro[posp].cod_proveedor);
		y++;	
			
			gotoxy(x,y);			
			printf("Razon social: %s\n",dat_pro[posp].razon);
			y++;
			
			gotoxy(x,y);
			printf("Telefono: %d\n",dat_pro[posp].tel);
			y++;
			
			gotoxy(x,y);
			printf("E-mail: %s\n",dat_pro[posp].mail);
			y++;
			
			gotoxy(x,y);
			printf("Direccion: %s\n",dat_pro[posp].direccion);
			y++;
			
		gotoxy(x,y);
		printf("Desea dar de baja este proveedor?");
		y++;
		
		gotoxy(x,y);
		printf("1_si\n");
		y++;
		gotoxy(x,y);
		printf("2_no\n");
		y++;
		gotoxy(x,y);
		scanf("%d",&op);
		fflush(stdin);
		y=5;
		system("cls");
		switch(op)
		{
			case 1:
				cuadro();
				dat_pro[posp].baja_logica=0;//inactivo
				gotoxy(x,y);
				printf("fue eliminado correctamente\n");
				getch();
				system("cls");
				menu_bajas();
				break;				
			case 2:
				system("cls");
				menu_bajas();
				break;
			default:
				cuadro();
				gotoxy(x,y);
				printf("ERROR en el ingreso\n");
				getch;
				system("cls");
				menu_bajas();
				break;
		}
	}
}
//--------------------------------------------------------------------------------------------------
int modificar_articulo()
{
	int coco;
	int i,o,n;	
	int opp=1;
	int cont=0;
	y=5;	
		
	while(opp==1)
	{	
		cuadro();
		gotoxy(x,y);
		printf("Ingrese c\xA2""digo del articulo a modificar: "); 
		y++;
		gotoxy(x,y);
		scanf("%d",&coco);
		fflush(stdin);
		system("cls");
		for(i=0;i<da;i++)
		{
			if(dat_art[i].baja_logica==1)
			{
				if(dat_art[i].cod_art==coco)
				{
							system("cls");
							cuadro();
							y=5;
							gotoxy(x,y);					
							printf("-----Datos del articulo-----\n");					
						
							y++;
							gotoxy(x,y);
							printf("1-c\xA2""digo articulo: %d\n",dat_art[i].cod_art);
							
							y++;
							gotoxy(x,y);
							printf("2-Descripcion: %s\n",dat_art[i].desc);
							
							y++;
							gotoxy(x,y);
							printf("3-Rubro: %s\n",dat_art[i].rubro);
							
							y++;
							gotoxy(x,y);
							printf("4-c\xA2""digo proveedor: %d\n",dat_art[i].cod_proveedor);
							
							y++;
							gotoxy(x,y);
							printf("5-Precio: %d\n",dat_art[i].precio);
							
							y++;
							gotoxy(x,y);
							printf("6-Todos los campos\n");
							
							y++;
							gotoxy(x,y);
							printf("7-Volver\n");
							
							y++;
							gotoxy(x,y);
							printf("Ingrese opcion a modificar\n");
						
							y++;
							gotoxy(x,y);
							scanf("%d",&o);
				fflush(stdin);
				y=5;
				system("cls");
				cont++;
				switch (o)
				{
					case 1:
						cuadro();
						gotoxy(x,y);
						printf("ingrese c\xA2""digo nuevo de articulo ");
						y++;
						gotoxy(x,y);////////////////////
						scanf("%d",&(dat_art[i].cod_art));
						fflush(stdin);
						y++;
						gotoxy(x,y);
						printf("\n\nDatos actualizados exitosamente!"); 
						getch();
						break;
					case 2:
						cuadro();
						gotoxy(x,y);
						printf("ingrese nueva descripcion");
						y++;
						gotoxy(x,y);////////////////////
						gets(dat_art[i].desc);
						fflush(stdin);
						y++;
						gotoxy(x,y);
						printf("\n\nDatos actualizados exitosamente!"); 
						getch();
						break;
					case 3:
						cuadro();
						gotoxy(x,y);
						printf("ingrese nuevo rubro");
						y++;
						gotoxy(x,y);////////////////////
						gets(dat_art[i].rubro);
						fflush(stdin);
						y++;
						gotoxy(x,y);
						printf("\n\nDatos actualizados exitosamente!"); 
						getch();
						break;
					case 4:
						cuadro();
						gotoxy(x,y);
						printf("ingrese c\xA2""digo nuevo del proveedor a modificar");
						y++;
						gotoxy(x,y);////////////////////
						scanf("%d",&(dat_art[i].cod_proveedor));
						fflush(stdin);
						y++;
						gotoxy(x,y);
						printf("\n\nDatos actualizados exitosamente!");
						getch();
						break;					
					case 5: 
						cuadro();
						gotoxy(x,y);
						printf("ingrese nuevo precio de articulo");
						y++;
						gotoxy(x,y);////////////////////
						scanf("%d",&(dat_art[i].precio));
						fflush(stdin);
						y++;
						gotoxy(x,y);
						printf("\n\nDatos actualizados exitosamente!"); 
						getch();
						break;
					case 6:
						cuadro();
						gotoxy(x,y);
						printf("ingrese c\xA2""digo nuevo de articulo ");
						y++;
						gotoxy(x,y);////////////////////
						scanf("%d",&(dat_art[i].cod_art));
						fflush(stdin);
						y++;
						gotoxy(x,y);
						printf("ingrese nueva descripcion");
						y++;
						gotoxy(x,y);////////////////////
						gets(dat_art[i].desc);
						fflush(stdin);
						y++;
						gotoxy(x,y);
						printf("ingrese nuevo rubro");
						y++;
						gotoxy(x,y);////////////////////
						gets(dat_art[i].rubro);
						fflush(stdin);
						y++;
						gotoxy(x,y);
						printf("ingrese c\xA2""digo nuevo del proveedor a modificar");
						y++;
						gotoxy(x,y);////////////////////
						scanf("%d",&(dat_art[i].cod_proveedor));
						fflush(stdin);
						y++;
						gotoxy(x,y);
						printf("ingrese nuevo precio de articulo");
						y++;
						gotoxy(x,y);////////////////////
						scanf("%d",&(dat_art[i].precio));
						fflush(stdin);
						y++;
						gotoxy(x,y);
						printf("\n\nDatos actualizados exitosamente!"); 
						getch();
						break;
						
						case 7:
						menu_modificaciones();
						break;
	
					default:
						system("cls");
						printf("ERROR AL INGRESO DE OPCION");
						getch();
						modificar_articulo();
						break;
						
				}//cierre switch	
				y=5;
				}//cierre del if
			}//cierre bandera
		}//cierre for
		if (cont==0)
		{
			cuadro();
			y=5;
			gotoxy(x,y);
			printf("no se encontraron resultados\n");
			y++;
			gotoxy(x,y);
			printf("�desea volver a intentarlo?\n");
			y++;
			gotoxy(x,y);
			printf("1_si\n");
			y++;
			gotoxy(x,y);
			printf("2_no\n");
			y++;
			gotoxy(x,y);
			scanf("%d",&n);
			fflush(stdin);
			y=5;
			if(n==1)
			{
				system("cls");
				modificar_articulo();
			}
			system("cls");
			menu_modificaciones();
		}
		system("cls");
		cuadro();
		gotoxy(x,y);
		printf("�Desea modificar otro dato? 1-si o 2-no");
		y++;
		gotoxy(x,y);
		scanf("%d",&opp);
		fflush(stdin);
		system("cls");
		y=5;
		if(opp==2)
		{
			system("cls");
			menu_modificaciones();
		}
	}//cierre while
}
//--------------------------------------------------------------------------------------------------
int modificar_proveedor()
{
	int coc;
	int i,o,n,pp;	
	int opp=1;
	int cont=0;
	y=5;	
	int y=5;
	int x=20;	
	while(opp==1)
	{	
		cuadro();
		system("cls");
				//principio cartel detalle
				pp=8;
				gotoxy(58,5);
				printf("c\xA2""digo");
				gotoxy(58,6);
				printf("proveedor");
				gotoxy(56,4);
				printf("�������������");
				gotoxy(56,6);
				printf("�");
				gotoxy(68,6);
				printf("�");
				gotoxy(56,5);
				printf("�");
				gotoxy(68,5);
				printf("�");
				gotoxy(56,7);
				printf("�������������");
				for(i=0;i<dp;i++)//cartel indicacion de ventas existente
				{
					if(dat_pro[i].baja_logica==1)
					{
						gotoxy(68,pp);
						printf("�");
						gotoxy(56,pp);
						printf("�");
						gotoxy(58,pp);
						printf("%d",dat_pro[i].cod_proveedor);
						pp++;
					}
				}
		gotoxy(x,y);
		printf("Ingrese c\xA2""digo del proveedor a modificar: "); 
		y++;
		gotoxy(x,y);
		scanf("%d",&coc);
		fflush(stdin);
		system("cls");
		for(i=0;i<dp;i++)
		{
			if(dat_pro[i].baja_logica==1)
			{
				if(dat_pro[i].cod_proveedor==coc)
				{
							system("cls");
							cuadro();
							y=5;
							gotoxy(x,y);					
							printf("-----Datos del proveedor-----\n");					
						
							y++;
							gotoxy(x,y);
							printf("1-c\xA2""digo proveedor: %d\n",dat_pro[i].cod_proveedor);
							
							y++;
							gotoxy(x,y);
							printf("2-Razon social: %s\n",dat_pro[i].razon);
							
							y++;
							gotoxy(x,y);
							printf("3-Telefono: %d\n",dat_pro[i].tel);
							
							y++;
							gotoxy(x,y);
							printf("4-E-mail: %s\n",dat_pro[i].mail);
							
							y++;
							gotoxy(x,y);
							printf("5-Direccion: %s\n",dat_pro[i].direccion);
							
							y++;
							gotoxy(x,y);
							printf("6-Todos los campos\n");
							
							y++;
							gotoxy(x,y);
							printf("7-Volver");
							
							y++;
							gotoxy(x,y);
							printf("Ingrese opcion a modificar\n");
						
							y++;
							gotoxy(x,y);
							scanf("%d",&o);
				fflush(stdin);
				y=5;
				system("cls");
				cont++;
				switch (o)
				{
					case 1:
						cuadro();
						gotoxy(x,y);
						printf("ingrese c\xA2""digo nuevo del proveedor ");
						y++;
						gotoxy(x,y);////////////////////
						scanf("%d",&(dat_pro[i].cod_proveedor));
						fflush(stdin);
						y++;
						gotoxy(x,y);
						printf("\n\nDatos actualizados exitosamente!"); 
						getch();
						break;
					case 2:
						cuadro();
						gotoxy(x,y);
						printf("ingrese nueva razon social");
						y++;
						gotoxy(x,y);////////////////////
						gets(dat_pro[i].razon);
						fflush(stdin);
						y++;
						gotoxy(x,y);
						printf("\n\nDatos actualizados exitosamente!"); 
						getch();
						break;
					case 3:
						cuadro();
						gotoxy(x,y);
						printf("ingrese nuevo telefono");
						y++;
						gotoxy(x,y);////////////////////
						scanf("%d",&(dat_pro[i].tel));
						fflush(stdin);
						y++;
						gotoxy(x,y);
						printf("\n\nDatos actualizados exitosamente!"); 
						getch();
						break;
					case 4:
						cuadro();
						gotoxy(x,y);
						printf("ingrese nuevo E-mail");
						y++;
						gotoxy(x,y);////////////////////
						gets(dat_pro[i].mail);
						fflush(stdin);
						y++;
						gotoxy(x,y);
						printf("\n\nDatos actualizados exitosamente!");
						getch();
						break;					
					case 5: 
						cuadro();
						gotoxy(x,y);
						printf("ingrese nueva direccion");
						y++;
						gotoxy(x,y);////////////////////
						gets(dat_pro[i].direccion);
						fflush(stdin);
						y++;
						gotoxy(x,y);
						printf("\n\nDatos actualizados exitosamente!"); 
						getch();
						break;
					case 6:
						cuadro();
						gotoxy(x,y);
						printf("ingrese c\xA2""digo nuevo del proveedor ");
						y++;
						gotoxy(x,y);////////////////////
						scanf("%d",&(dat_pro[i].cod_proveedor));
						fflush(stdin);
						
						y++;
						gotoxy(x,y);
						printf("ingrese nueva razon social");
						y++;
						gotoxy(x,y);////////////////////
						gets(dat_pro[i].razon);
						fflush(stdin);
						
						y++;
						gotoxy(x,y);
						printf("ingrese nuevo telefono");
						y++;
						gotoxy(x,y);////////////////////
						scanf("%d",&(dat_pro[i].tel));
						fflush(stdin);
						
						y++;
						gotoxy(x,y);
						printf("ingrese nuevo E-mail");
						y++;
						gotoxy(x,y);////////////////////
						gets(dat_pro[i].mail);
						fflush(stdin);
						
						y++;
						gotoxy(x,y);
						printf("ingrese nueva direccion");
						y++;
						gotoxy(x,y);////////////////////
						gets(dat_pro[i].direccion);
						fflush(stdin);
						
						y++;
						gotoxy(x,y);
						printf("\n\nDatos actualizados exitosamente!"); 
						getch();
						break;
						
					case 7:
						menu_modificaciones();
						break;
					
					
					default:
						system("cls");
						printf("ERROR AL INGRESO DE OPCION");
						getch();
						modificar_proveedor();
						break;
				}//cierre switch	
				y=5;
				}//cierre del if
			}//cierre bandera
		}//cierre for
		if (cont==0)
		{
			cuadro();
			y=5;
			gotoxy(x,y);
			printf("no se encontraron resultados\n");
			y++;
			gotoxy(x,y);
			printf("�desea volver a intentarlo?\n");
			y++;
			gotoxy(x,y);
			printf("1_si\n");
			y++;
			gotoxy(x,y);
			printf("2_no\n");
			y++;
			gotoxy(x,y);
			scanf("%d",&n);
			fflush(stdin);
			y=5;
			if(n==1)
			{
				system("cls");
				modificar_proveedor();
			}
			system("cls");
			menu_modificaciones();
		}
		system("cls");
		cuadro();
		gotoxy(x,y);
		printf("�Desea modificar otro dato? 1-si o 2-no");
		y++;
		gotoxy(x,y);
		scanf("%d",&opp);
		fflush(stdin);
		system("cls");
		y=5;
		if(opp==2)
		{
			system("cls");
			menu_modificaciones();
		}
	}//cierre while
}
//--------------------------------------------------------------------------------------------------
int buscar_proveedor()
{
	int cod_proveedor;
	char razon[30];
	char tel[30];
	char mail[30];
	char direccion[30];
		int cont=0;
		int oo,i,po=1;
				system("cls");

			y=5;
		cuadro();
		gotoxy(x,y);
		printf("1-Buscar atravez del c\xA2""digo del proveedor\n");
		y++;
		gotoxy(x,y);
		printf("2-Buscar atravez de la razon social\n");
		y++;
		gotoxy(x,y);
		printf("3-Buscar atravez del telefono\n");
		y++;
		gotoxy(x,y);	
		printf("4-Buscar atravez del correo\n");
		y++;
		gotoxy(x,y);
		printf("5-Buscar atravez de la direccion\n");
		y++;
		gotoxy(x,y);
		printf("6-Volver\n");
		y++;
		gotoxy(x,y);
		scanf("%d",&oo);
		switch(oo)
		{
			case 1:
				while(po==1)
				{
					cont=0;
					y=5;
					system("cls");
					cuadro();
					gotoxy(x,y);
					printf("Ingrese c\xA2""digo\n");
					y++;
					gotoxy(x,y);
					scanf("%d",&cod_proveedor);
					for(i=0;i<dp;i++)
					{
						if(dat_pro[i].baja_logica==1)
						{
							if(dat_pro[i].cod_proveedor==cod_proveedor)	
							{	
							system("cls");
							cuadro();
							y=5;
							gotoxy(x,y);					
							printf("-----Datos del proveedor-----\n");					
							y++;
							gotoxy(x,y);
							printf("c\xA2""digo proveedor: %d\n",dat_pro[i].cod_proveedor);
							y++;
							gotoxy(x,y);
							printf("Razon social: %s\n",dat_pro[i].razon);
							y++;
							gotoxy(x,y);
							printf("Telefono: %d\n",dat_pro[i].tel);
							y++;
							gotoxy(x,y);
							printf("Correo: %s\n",dat_pro[i].mail);
							y++;
							gotoxy(x,y);
							printf("Direccion: %s\n",dat_pro[i].direccion);
							getch();
							cont++;
							}
						}
					if(cont==0)
						{
							
							system("cls");
							cuadro();
							gotoxy(x,6);
							printf("c\xA2""digo no encontrado!");
													
						}
					}
				gotoxy(x,13);
				printf("�Desea Buscar otro provedor atravez del c\xA2""digo?");
				gotoxy(x,14);
				printf("1-si");
				gotoxy(x,15);
				printf("2-no");
				gotoxy(x,16);
				scanf("%d",&po);
				}
				buscar_proveedor();
				break;
			case 2:
				while(po==1)
				{
					cont=0;
					y=5;
					system("cls");
					cuadro();
					gotoxy(x,y);
					printf("Ingrese razon social\n");
					y++;
					gotoxy(x,y);
					scanf("%s",razon);
					for(i=0;i<dp;i++)
					{
						if(dat_pro[i].baja_logica==1)
						{
							if(!strcmp(dat_pro[i].razon,razon))
							{	
							
							cuadro();
							y=5;
							gotoxy(x,y);					
							printf("-----Datos del proveedor-----\n");					
							y++;
							gotoxy(x,y);
							printf("c\xA2""digo proveedor: %d\n",dat_pro[i].cod_proveedor);
							y++;
							gotoxy(x,y);
							printf("Razon social: %s\n",dat_pro[i].razon);
							y++;
							gotoxy(x,y);
							printf("Telefono: %d\n",dat_pro[i].tel);
							y++;
							gotoxy(x,y);
							printf("Correo: %s\n",dat_pro[i].mail);
							y++;
							gotoxy(x,y);
							printf("Direccion: %s\n",dat_pro[i].direccion);
							getch();
							cont++;
							}
						}
					if(cont==0)
						{
							
							system("cls");
							cuadro();
							gotoxy(x,6);
							printf("Razon social no encontrada!");
													
						}
					}
				gotoxy(x,13);
				printf("�Desea Buscar otro articulo atravez de la razon?");
				gotoxy(x,14);
				printf("1-si");
				gotoxy(x,15);
				printf("2-no");
				gotoxy(x,16);
				scanf("%d",&po);
				}
				buscar_proveedor();
				break;
			case 3:
				while(po==1)
				{
					cont=0;
					y=5;
					system("cls");
					cuadro();
					gotoxy(x,y);
					printf("Ingrese telefono\n");
					y++;
					gotoxy(x,y);
					gets(tel);
					for(i=0;i<dp;i++)
					{
						if(dat_pro[i].baja_logica==1)
						{
							if(!strcmp(dat_pro[i].tel,tel))
							{
							system("cls");
							cuadro();
							y=5;
							gotoxy(x,y);					
							printf("-----Datos del proveedor-----\n");					
							y++;
							gotoxy(x,y);
							printf("c\xA2""digo proveedor: %d\n",dat_pro[i].cod_proveedor);
							y++;
							gotoxy(x,y);
							printf("Razon social: %s\n",dat_pro[i].razon);
							y++;
							gotoxy(x,y);
							printf("Telefono: %s\n",dat_pro[i].tel);
							y++;
							gotoxy(x,y);
							printf("Correo: %s\n",dat_pro[i].mail);
							y++;
							gotoxy(x,y);
							printf("Direccion: %s\n",dat_pro[i].direccion);
							cont++;
							}
						}
					if(cont==0)
						{
							
							system("cls");
							cuadro();
							gotoxy(x,6);
							printf("telefono no encontrado!");
													
						}
					}
				gotoxy(x,13);
				printf("�Desea Buscar otro proveedor atravez del telefono?");
				gotoxy(x,14);
				printf("1-si");
				gotoxy(x,15);
				printf("2-no");
				gotoxy(x,16);
				scanf("%d",&po);
				}
				buscar_proveedor();
				break;
			case 4:
				while(po==1)
				{
					cont=0;
					y=5;
					system("cls");
					cuadro();
					gotoxy(x,y);
					printf("Ingrese correo\n");
					y++;
					gotoxy(x,y);
					scanf("%s",mail);
					for(i=0;i<dp;i++)
					{
						if(dat_pro[i].baja_logica==1)
						{
							if(!strcmp(dat_pro[i].mail,mail))
							{
							system("cls");
							cuadro();
							y=5;
							gotoxy(x,y);					
							printf("-----Datos del proveedor-----\n");					
							y++;
							gotoxy(x,y);
							printf("c\xA2""digo proveedor: %d\n",dat_pro[i].cod_proveedor);
							y++;
							gotoxy(x,y);
							printf("Razon social: %s\n",dat_pro[i].razon);
							y++;
							gotoxy(x,y);
							printf("Telefono: %d\n",dat_pro[i].tel);
							y++;
							gotoxy(x,y);
							printf("Correo: %s\n",dat_pro[i].mail);
							y++;
							gotoxy(x,y);
							printf("Direccion: %s\n",dat_pro[i].direccion);
							cont++;
							}
						}
					if(cont==0)
						{
							
							system("cls");
							cuadro();
							gotoxy(x,6);
							printf("Correo no encontrado!");
													
						}
					}
				gotoxy(x,13);
				printf("�Desea Buscar otro proveedor atravez del correo?");
				gotoxy(x,14);
				printf("1-si");
				gotoxy(x,15);
				printf("2-no");
				gotoxy(x,16);
				scanf("%d",&po);
				}
				buscar_proveedor();
				break;
			case 5:
				while(po==1)
				{
					cont=0;
					y=5;
					system("cls");
					cuadro();
					gotoxy(x,y);
					printf("Ingrese Direccion\n");
					y++;
					gotoxy(x,y);
					scanf("%s",direccion);
					for(i=0;i<dp;i++)
					{
						if(dat_pro[i].baja_logica==1)
						{
							if(!strcmp(dat_pro[i].direccion,direccion))
							{			
							system("cls");
							cuadro();
							y=5;
							gotoxy(x,y);					
							printf("-----Datos del proveedor-----\n");					
							y++;
							gotoxy(x,y);
							printf("c\xA2""digo proveedor: %d\n",dat_pro[i].cod_proveedor);
							y++;
							gotoxy(x,y);
							printf("Razon social: %s\n",dat_pro[i].razon);
							y++;
							gotoxy(x,y);
							printf("Telefono: %d\n",dat_pro[i].tel);
							y++;
							gotoxy(x,y);
							printf("Correo: %s\n",dat_pro[i].mail);
							y++;
							gotoxy(x,y);
							printf("Direccion: %s\n",dat_pro[i].direccion);
							getch();
							cont++;
							}
						}
						if(cont==0)
						{
							
							system("cls");
							cuadro();
							gotoxy(x,6);
							printf("Direccion no encontrada!");
													
						}
					}
				gotoxy(x,13);
				printf("�Desea Buscar otro proveedor atravez de la direccion?");
				gotoxy(x,14);
				printf("1-si");
				gotoxy(x,15);
				printf("2-no");
				gotoxy(x,16);
				scanf("%d",&po);
				}
				buscar_proveedor();
				break;
			case 6:
				system("cls");
				menu_buscar();
				break;
			default:
				system("cls");
			cuadro;
			printf("error al ingreso de  opcion");
			menu_buscar();
			break;
				
		}
	}
//--------------------------------------------------------------------------------------------------
int buscar_articulo()
{
	int opp,i,o=1;
	int cod_art;
	int cont=0;
	char Desc[30];
	char rubro[30];
	char precio[30];
	int cod_proveedor;
	y=5;
		system("cls");
		cuadro();
		gotoxy(x,y);
		printf("1-Buscar atravez del c\xA2""digo de articulo\n");
		y++;
		gotoxy(x,y);
		printf("2-Buscar atravez de la descripcion\n");
		y++;
		gotoxy(x,y);
		printf("3-Buscar atravez de rubro\n");
		y++;
		gotoxy(x,y);	
		printf("4-Buscar atravez del precio del articulo\n");
		y++;
		gotoxy(x,y);
		printf("5-Buscar atravez del c\xA2""digo del proveedor\n");
		y++;
		gotoxy(x,y);
		printf("6-Volver\n");
		y++;
		gotoxy(x,y);
		scanf("%d",&opp);
	switch(opp)
	{
		case 1: 
			while(o==1)
			{	
				cont=0;
				y=5;
				system("cls");
				cuadro();
				gotoxy(x,y);
				printf("Ingrese c\xA2""digo\n");
				y++;
				gotoxy(x,y);
				scanf("%d",&cod_art);
				for(i=0;i<da;i++)
				{
					if(dat_art[i].baja_logica==1)
					{	
						if(dat_art[i].cod_art==cod_art)
						{	
							system("cls");
							cuadro();
							
							y=5;
							gotoxy(x,y);					
							printf("-----Datos de articulo-----\n");					
							y++;
							gotoxy(x,y);
							printf("c\xA2""digo articulo: %d\n",dat_art[i].cod_art);
							y++;
							gotoxy(x,y);
							printf("Descripcion: %s\n",dat_art[i].desc);
							y++;
							gotoxy(x,y);
							printf("Rubro: %s\n",dat_art[i].rubro);
							y++;
							gotoxy(x,y);
							printf("c\xA2""digo proveedor: %d\n",dat_art[i].cod_proveedor);
							y++;
							gotoxy(x,y);
							printf("Precio: %d\n",dat_art[i].precio);
							cont++;
						}
					}
					if(cont==0)
						{
							
							system("cls");
							cuadro();
							gotoxy(x,6);
							printf("c\xA2""digo no encontrado!");
													
						}
				}
				gotoxy(x,13);
				printf("�Desea Buscar otro articulo atravez del c\xA2""digo?");
				gotoxy(x,14);
				printf("1-si");
				gotoxy(x,15);
				printf("2-no");
				gotoxy(x,16);
				scanf("%d",&o);
			}//fin de while
			buscar_articulo();
			break;
		case 2:
			while(o==1)
			{	
				cont=0;
				y=5;
				system("cls");
				cuadro();
				gotoxy(x,y);
				printf("Ingrese Descripcion\n");
				y++;
				gotoxy(x,y);
				scanf("%s",Desc);
				fflush(stdin);
				system("cls");
				for(i=0;i<da;i++)
				{
					if(dat_art[i].baja_logica==1)
					{
						if(!strcmp(dat_art[i].desc,Desc))
						{	
							system("cls");
							cuadro();
							y=5;
							gotoxy(x,y);					
							printf("-----Datos de articulo-----\n");					
							y++;
							gotoxy(x,y);
							printf("c\xA2""digo articulo: %d\n",dat_art[i].cod_art);
							y++;
							gotoxy(x,y);
							printf("Descripcion: %s\n",dat_art[i].desc);
							y++;
							gotoxy(x,y);
							printf("Rubro: %s\n",dat_art[i].rubro);
							y++;
							gotoxy(x,y);
							printf("c\xA2""digo proveedor: %d\n",dat_art[i].cod_proveedor);
							y++;
							gotoxy(x,y);
							printf("Precio: %d\n",dat_art[i].precio);
							cont++;
						}
					}
					if(cont==0)
						{
							
							system("cls");
							cuadro();
							gotoxy(x,6);
							printf("Descripcion no encontrada!");
													
						}
				}
				gotoxy(x,13);
				printf("�Desea Buscar otro articulo atravez de la descripcion?");
				gotoxy(x,14);
				printf("1-si");
				gotoxy(x,15);
				printf("2-no");
				gotoxy(x,16);
				scanf("%d",&o);
			}
			buscar_articulo();
			break;
		case 3:
			while(o==1)
			{	
				cont=0;
				y=5;
				system("cls");
				cuadro();
				gotoxy(x,y);
				printf("Ingrese rubro\n");
				y++;
				gotoxy(x,y);
				scanf("%s",rubro);
				for(i=0;i<da;i++)
				{
					if(dat_art[i].baja_logica==1)
					{
						if(!strcmp(dat_art[i].rubro,rubro))
						{	
							system("cls");
							cuadro();
							
							y=5;
							gotoxy(x,y);					
							printf("-----Datos de articulo-----\n");					
							y++;
							gotoxy(x,y);
							printf("c\xA2""digo articulo: %d\n",dat_art[i].cod_art);
							y++;
							gotoxy(x,y);
							printf("Descripcion: %s\n",dat_art[i].desc);
							y++;
							gotoxy(x,y);
							printf("Rubro: %s\n",dat_art[i].rubro);
							y++;
							gotoxy(x,y);
							printf("c\xA2""digo proveedor: %d\n",dat_art[i].cod_proveedor);
							y++;
							gotoxy(x,y);
							printf("Precio: %d\n",dat_art[i].precio);
							cont++;
						}
					}
					if(cont==0)
						{
							
							system("cls");
							cuadro();
							gotoxy(x,6);
							printf("Rubro no encontrado!");
													
						}
				}
				gotoxy(x,13);
				printf("�Desea Buscar otro articulo atravez del rubro?");
				gotoxy(x,14);
				printf("1-si");
				gotoxy(x,15);
				printf("2-no");
				gotoxy(x,16);
				scanf("%d",&o);
			}
			buscar_articulo();
			break;
		case 4:
			while(o==1)
			{	
				cont=0;
				y=5;
				system("cls");
				cuadro();
				gotoxy(x,y);
				fflush(stdin);
				printf("Ingrese precio\n");
				y++;
				gotoxy(x,y);
				gets(precio);
				for(i=0;i<da;i++)
				{
					if(dat_art[i].baja_logica==1)
					{
						if(!strcmp(dat_art[i].precio,precio))
						{	
							system("cls");
							cuadro();
							
							y=5;
							gotoxy(x,y);					
							printf("-----Datos de articulo-----\n");					
							y++;
							gotoxy(x,y);
							printf("c\xA2""digo articulo: %d\n",dat_art[i].cod_art);
							y++;
							gotoxy(x,y);
							printf("Descripcion: %s\n",dat_art[i].desc);
							y++;
							gotoxy(x,y);
							printf("Rubro: %s\n",dat_art[i].rubro);
							y++;
							gotoxy(x,y);
							printf("c\xA2""digo proveedor: %d\n",dat_art[i].cod_proveedor);
							y++;
							gotoxy(x,y);
							printf("Precio: %d\n",dat_art[i].precio);
							cont++;
						}
					}
					if(cont==0)
						{
							
							system("cls");
							cuadro();
							gotoxy(x,6);
							printf("Precio no encontrado!");
													
						}
				}
				gotoxy(x,13);
				printf("�Desea Buscar otro articulo atravez del precio?");
				gotoxy(x,14);
				printf("1-si");
				gotoxy(x,15);
				printf("2-no");
				gotoxy(x,16);
				scanf("%d",&o);
			}
			buscar_articulo();
			break;
		case 5:
			while(o==1)
			{	
				cont=0;
				y=5;
				system("cls");
				cuadro();
				gotoxy(x,y);
				printf("Ingrese c\xA2""digo proveedor\n");
				y++;
				gotoxy(x,y);
				scanf("%d",&cod_proveedor);
				for(i=0;i<da;i++)
				{
					if(dat_art[i].baja_logica==1)
					{
						if(dat_art[i].cod_proveedor==cod_proveedor)
						{	
							system("cls");
							cuadro();
							
							y=5;
							gotoxy(x,y);					
							printf("-----Datos de articulo-----\n");					
							y++;
							gotoxy(x,y);
							printf("c\xA2""digo articulo: %d\n",dat_art[i].cod_art);
							y++;
							gotoxy(x,y);
							printf("Descripcion: %s\n",dat_art[i].desc);
							y++;
							gotoxy(x,y);
							printf("Rubro: %s\n",dat_art[i].rubro);
							y++;
							gotoxy(x,y);
							printf("c\xA2""digo proveedor: %d\n",dat_art[i].cod_proveedor);
							y++;
							gotoxy(x,y);
							printf("Precio: %d\n",dat_art[i].precio);
							cont++;
						}
					}
					if(cont==0)
						{
							
							system("cls");
							cuadro();
							gotoxy(x,6);
							printf("c\xA2""digo de proveedor no encontrado!");
													
						}
				}
				gotoxy(x,13);
				printf("�Desea Buscar otro articulo atravez del c\xA2""digo del proveedor?");
				gotoxy(x,14);
				printf("1-si");
				gotoxy(x,15);
				printf("2-no");
				gotoxy(x,16);
				scanf("%d",&o);
			}
			buscar_articulo();
			break;
		case 6:
			system("cls");
			menu_buscar();
			break;
		default:
			system("cls");
			cuadro;
			printf("error al ingreso de  opcion");
			menu_buscar();
			break;
			
	}
}//fin busqueda articulos

int lista_articulos_prov(int cod_prov)
{

	int i;
	int y=5;
	gotoxy(0,4);
	printf("��������������������������������������������������������������������������������");
	gotoxy(2,y);
	printf("Cod A\n");
	gotoxy(10,y);
	printf("Descripcion");
	gotoxy(30,y);
	printf("Rubro\n");
	gotoxy(47,y);
	printf("Cod P\n");
	gotoxy(57,y);
	printf("Precio\n");

	gotoxy(0,y);
	printf("�");//linea 1 izquierda
	gotoxy(8,y);
	printf("�");//linea 2 izquierda
	gotoxy(28,y);
	printf("�");//linea 3 izquierda
	gotoxy(43,y);
	printf("�");//linea 4 izquierda
	gotoxy(55,y);
	printf("�");//linea 5 izquierda
	gotoxy(79,y);
	printf("�");//linea 6 izquierda
	y++;
	gotoxy(0,y);
	printf("��������������������������������������������������������������������������������");
	for(i=0;i<da;i++)
	{
		
		if(dat_art[i].baja_logica==1 && dat_art[i].cod_proveedor == cod_prov)
		{	
			
			y++;
			gotoxy(2,y);
			printf("%d\n",dat_art[i].cod_art);
			gotoxy(10,y);
			printf("%s\n",dat_art[i].desc);
			gotoxy(30,y);
			printf("%s\n",dat_art[i].rubro);
			gotoxy(47,y);
			printf("%d\n",dat_art[i].cod_proveedor);
			gotoxy(57,y);
			printf("%s\n",dat_art[i].precio);

			gotoxy(0,y);
			printf("�");//linea 1 izquierda
			gotoxy(8,y);
			printf("�");//linea 2 izquierda
			gotoxy(28,y);
			printf("�");//linea 3 izquierda
			gotoxy(43,y);
			printf("�");//linea 4 izquierda
			gotoxy(55,y);
			printf("�");//linea 5 izquierda
			gotoxy(79,y);
			printf("�");//linea 6 izquierda
		}
	}
	printf("��������������������������������������������������������������������������������");
	getch();
	system("cls");
	menu_listas();
}

//--------------------------------------------------------------------------------------------------
int lista_articulos()
{

	int i;
	int y=1;
	gotoxy(0,0);
	printf("��������������������������������������������������������������������������������");
	gotoxy(2,y);
	printf("Cod A\n");
	gotoxy(10,y);
	printf("Descripcion");
	gotoxy(30,y);
	printf("Rubro\n");
	gotoxy(47,y);
	printf("Cod P\n");
	gotoxy(57,y);
	printf("Precio\n");

	gotoxy(0,y);
	printf("�");//linea 1 izquierda
	gotoxy(8,y);
	printf("�");//linea 2 izquierda
	gotoxy(28,y);
	printf("�");//linea 3 izquierda
	gotoxy(43,y);
	printf("�");//linea 4 izquierda
	gotoxy(55,y);
	printf("�");//linea 5 izquierda
	gotoxy(79,y);
	printf("�");//linea 6 izquierda
	y++;
	gotoxy(0,y);
	printf("��������������������������������������������������������������������������������");
	for(i=0;i<da;i++)
	{
		
		if(dat_art[i].baja_logica==1)
		{	
			y++;
			gotoxy(2,y);
			printf("%d\n",dat_art[i].cod_art);
			gotoxy(10,y);
			printf("%s\n",dat_art[i].desc);
			gotoxy(30,y);
			printf("%s\n",dat_art[i].rubro);
			gotoxy(47,y);
			printf("%d\n",dat_art[i].cod_proveedor);
			gotoxy(57,y);
			printf("%s\n",dat_art[i].precio);

			gotoxy(0,y);
			printf("�");//linea 1 izquierda
			gotoxy(8,y);
			printf("�");//linea 2 izquierda
			gotoxy(28,y);
			printf("�");//linea 3 izquierda
			gotoxy(43,y);
			printf("�");//linea 4 izquierda
			gotoxy(55,y);
			printf("�");//linea 5 izquierda
			gotoxy(79,y);
			printf("�");//linea 6 izquierda
		}
	}
	printf("��������������������������������������������������������������������������������");
	getch();
	system("cls");
	menu_listas();
}//cierre del proceso listado	
/*int sort_function( const void *a, const void *b)
{
return( strcmp((char *)a,(char *)b) );
}
*/
void ordenar_razon ()
{
	int primer, i, j, k; 
	
	for(i=0; i<dp; i++) 	
	{ 
		k=0; 	
		// recorrer asi mismo para validar con todas las Razon social
		for(j=0; j<dp; j++) 	
		{ 
			//strcmp devuelve 0 si son iguales depues devuelve < 0 si es menor y > 0 si la cadena es mayor 
			primer = strcmp(dat_pro[i].razon, dat_pro[j].razon); 
			if (primer>0) //si la cadena es mayor 
				k=k+1; 	
		}
		
		aux_prov[k] = dat_pro[i];
		
	}//fin del for para i 
	
}
//--------------------------------------------------------------------------------------------------
int lista_proveedor()
{
	
/*	int x;
	int qq;
	int resul;
	
	
	
	
	for (x=0;x<dp-1;x++)
	{
		for(qq=x+1;qq<dp;qq++)
		{
			resul=strcmp(dat_pro[x].razon,dat_pro[qq].razon);
			if (resul==1)
			{
				strcmp(auxx.razon,dat_pro[x].razon);
				strcmp(dat_pro[x].razon,dat_pro[qq].razon);
				strcmp(dat_pro[qq].razon,auxx.razon);
				
				auxx.cod_proveedor=dat_pro[x].cod_proveedor;
				dat_pro[x].cod_proveedor=dat_pro[qq].cod_proveedor;
				dat_pro[qq].cod_proveedor=auxx.cod_proveedor;
				
				strcmp(auxx.tel,dat_pro[x].tel);
				strcmp(dat_pro[x].tel,dat_pro[qq].tel);
				strcmp(dat_pro[qq].tel,auxx.tel);
				
				strcmp(auxx.mail,dat_pro[x].mail);
				strcmp(dat_pro[x].mail,dat_pro[qq].mail);
				strcmp(dat_pro[qq].mail,auxx.mail);
				
				strcmp(auxx.direccion,dat_pro[x].direccion);
				strcmp(dat_pro[x].direccion,dat_pro[qq].direccion);
				strcmp(dat_pro[qq].direccion,auxx.direccion);
				
			}
		}	
	}
*/	
	int i;
	int y=1;
	gotoxy(0,0);
	printf("��������������������������������������������������������������������������������");
	gotoxy(2,y);
	printf("Cod P\n");
	gotoxy(10,y);
	printf("Razon S");
	gotoxy(20,y);
	printf("Tel\n");
	gotoxy(30,y);
	printf("Correo\n");
	gotoxy(57,y);
	printf("Direccion\n");

	gotoxy(0,y);
	printf("�");//linea 1 izquierda
	gotoxy(8,y);
	printf("�");//linea 2 izquierda
	gotoxy(18,y);
	printf("�");//linea 3 izquierda
	gotoxy(28,y);
	printf("�");//linea 4 izquierda
	gotoxy(55,y);
	printf("�");//linea 5 izquierda
	gotoxy(79,y);
	printf("�");//linea 6 izquierda
	y++;
	gotoxy(0,y);
	printf("��������������������������������������������������������������������������������");

	ordenar_razon();

	for(i=0;i<dp;i++)
	{
		
		if(dat_pro[i].baja_logica==1)
		{
			y++;
			gotoxy(2,y);
			printf("%d\n",aux_prov[i].cod_proveedor);
			gotoxy(10,y);			
			printf("%s\n",aux_prov[i].razon);
			gotoxy(20,y);
			printf("%s\n",aux_prov[i].tel);
			gotoxy(30,y);
			printf("%s\n",aux_prov[i].mail);
			gotoxy(57,y);
			printf("%s\n",aux_prov[i].direccion);

			gotoxy(0,y);
			printf("�");//linea 1 izquierda
			gotoxy(8,y);
			printf("�");//linea 2 izquierda
			gotoxy(18,y);
			printf("�");//linea 3 izquierda
			gotoxy(28,y);
			printf("�");//linea 4 izquierda
			gotoxy(55,y);
			printf("�");//linea 5 izquierda
			gotoxy(79,y);
			printf("�");//linea 6 izquierda
		}
	}
	printf("��������������������������������������������������������������������������������");
	getch();
	system("cls");
	menu_listas();
}//cierre del proceso listado Proveedor	

//--------------------------------------------------------------------------------------------------
int lista_pro_art()
{
	int j,i,s=0,c=0,cp;
	int x=40,y=5;
	int pp=8;
	cuadro();
	while(s==0)
	{	
		system("cls");
				cuadro();
								//principio cartel detalle
				pp=8;
				gotoxy(58,5);
				printf("c\xA2""digo");
				gotoxy(58,6);
				printf("proveedor");
				gotoxy(56,4);
				printf("�������������");
				gotoxy(56,6);
				printf("�");
				gotoxy(68,6);
				printf("�");
				gotoxy(56,5);
				printf("�");
				gotoxy(68,5);
				printf("�");
				gotoxy(56,7);
				printf("�������������");
				for(i=0;i<dp;i++)//cartel indicacion de ventas existente
				{
					if(dat_pro[i].baja_logica==1)
					{
						gotoxy(68,pp);
						printf("�");
						gotoxy(56,pp);
						printf("�");
						gotoxy(58,pp);
						printf("%d",dat_pro[i].cod_proveedor);
						pp++;
					}
				}
		gotoxy(20,5);
		printf("Ingrese c\xA2""digo de proveedor\n");
		gotoxy(20,6);
		scanf("%d",&cp);

		for(i=0;i<dp;i++)
		{
			if(dat_pro[i].baja_logica==1)
			{
				if(dat_pro[i].cod_proveedor==cp)
					{
						system("cls");
						//cuadro();
						//gotoxy(10,0);
						printf("c\xA2""digo Prov. : %d\n",dat_pro[i].cod_proveedor);
						//gotoxy(10,1);
						printf("Razon Social : %s",dat_pro[i].razon);
						//getch();
						s++;
					}
			}
		}
		if(s==0)
		{
			system("cls");
			cuadro();
			gotoxy(20,8);
			printf("No se encontro el c\xA2""digo");
			getch();
		}
	}
	if(s>0)
	{
		lista_articulos_prov(cp); 
	}
}
//--------------------------------------------------------------------------------------------------
int cuadro()
{
	int i;
	int j=0;
	
	// si es negro cambia de color
	if (col == 0) 
		col++;
	
	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),col);
	
	col++;
	for(i=0;i<50;i++)
	{
		j=j+3;
		col++;
		if(j<68)
		{
			gotoxy(5+j,2);
			printf("�");
			gotoxy(5+j,22);
			printf("�");
			//
			gotoxy(5+j,1);
			printf("�");
			gotoxy(5+j,23);
			printf("�");
			//
			if(j<23)
			{
				gotoxy(8,j-1);
				printf("�");
				gotoxy(72,j-1);
				printf("�");
				//
				gotoxy(7,j-1);
				printf("�");
				gotoxy(73,j-1);
				printf("�");
				//
			}
		}
	}
	j=1;
	for(i=0;i<50;i++)
	{
		j=j+3;
		if(j<68)
		{
			gotoxy(5+j,2);
			printf("�");
			gotoxy(5+j,22);
			printf("�");
			//
			gotoxy(5+j,0);
			printf("�");
			gotoxy(5+j,24);
			printf("�");
			//
			if(j<23)
			{
				gotoxy(8,j-1);
				printf("�");
				gotoxy(72,j-1);
				printf("�");
				//
				gotoxy(6,j-1);
				printf("�");
				gotoxy(74,j-1);
				printf("�");
				//
			}
		}
	}
	j=2;
	for(i=0;i<50;i++)
	{
		j=j+3;
		if(j<68)
		{
			gotoxy(5+j,2);
			printf("�");
			gotoxy(5+j,22);
			printf("�");
			//
			gotoxy(5+j,0);
			printf("�");
			gotoxy(5+j,24);
			printf("�");
			//
			if(j<23)
			{
				gotoxy(8,j-1);
				printf("�");
				gotoxy(72,j-1);
				printf("�");
				//
				gotoxy(6,j-1);
				printf("�");
				gotoxy(74,j-1);
				printf("�");
				//
			}
		}
	}
	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),15);
}
//--------------------------------------------------------------------------------------------------

int cuadroanimado()
{
	int i;
	int j=0;
	if (col == 0) 
		col++;
	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),col);
	col++;
	for(i=0;i<100000000;i++)
	{
		j=j+3;
		if(j<68)
		{
			gotoxy(5+j,2);
			printf("�");
			gotoxy(5+j,22);
			printf("�");
			//
			gotoxy(5+j,1);
			printf("�");
			gotoxy(5+j,23);
			printf("�");
			//
			if(j<23)
			{
				gotoxy(8,j-1);
				printf("�");
				gotoxy(72,j-1);
				printf("�");
				//
				gotoxy(7,j-1);
				printf("�");
				gotoxy(73,j-1);
				printf("�");
				//
			}
		}
	}
	j=1;
	for(i=0;i<100000000;i++)
	{
		j=j+3;
		if(j<68)
		{
			gotoxy(5+j,2);
			printf("�");
			gotoxy(5+j,22);
			printf("�");
			//
			gotoxy(5+j,0);
			printf("�");
			gotoxy(5+j,24);
			printf("�");
			//
			if(j<23)
			{
				gotoxy(8,j-1);
				printf("�");
				gotoxy(72,j-1);
				printf("�");
				//
				gotoxy(6,j-1);
				printf("�");
				gotoxy(74,j-1);
				printf("�");
				//
			}
		}
	}
	j=2;
	for(i=0;i<100000000;i++)
	{
		j=j+3;
		if(j<68)
		{
			gotoxy(5+j,2);
			printf("�");
			gotoxy(5+j,22);
			printf("�");
			//
			gotoxy(5+j,0);
			printf("�");
			gotoxy(5+j,24);
			printf("�");
			//
			if(j<23)
			{
				gotoxy(8,j-1);
				printf("�");
				gotoxy(72,j-1);
				printf("�");
				//
				gotoxy(6,j-1);
				printf("�");
				gotoxy(74,j-1);
				printf("�");
				//
			}
		}
	}
	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),15);
}
//
