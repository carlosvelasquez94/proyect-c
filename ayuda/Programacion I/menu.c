//Librer�as.
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <windows.h> 
#include <ctype.h>
#define Usu 2 //Declaraci�n de una constante.
#define v 50
//Variables Globales.
int dp=2;
int da=2;
int pos_usu; 
int y=5;
int perfil_usu;
int i=0;

// M�todos.
int Presentacion(void);
int Cargando(void);
int Login (void);
int LoginAdmin (void);
int MenuAdmin (void);
int Alta (void); 
int AltaPelicula (void);
int AltaActor (void);
int Modificar (void);
int ModificarPelicula(void);
int ModificarActor(void);
int Listar (void);
int ListaPelicula (void);
int ListarActor (void);
int Baja (void);
int BajaPelicula (void);
int BajaActor (void);
int Buscar(void);
int BuscarPelicula(void);
int BuscarPeliculaCodigo(void);
int BuscarPeliculaTitulo(void);
int BuscarPeliculaAnio(void);
int BuscarPeliculaActorP(void);
int BuscarPeliculaGenero(void);
int BuscarActor(void);
int Salir (void);
int MenuUsu(void);
int ListarUsu(void);
int ListaPeliculaUsu (void);
int ListarActorUsu (void);
int BuscarUsu(void);

int ValidarLetras(char cadena[50]);
int ValidarNumeros(char numero[50]);
int ValidarDecimal(char decimal[50]);

int IdPelicula = 3;
int IdActor = 3;

//Estructuras.
struct DatosPelicula
{
int CodigoPelicula;
char Titulo[30];
char AnioDeEstreno[30];
char ActorPrincipal[30];
char Genero[30]; 
char Precio[30];
int BajaPelicula; //1-Activo 2-Baja (bandera)
};
 
struct DatosActor
{
int CodigoDeActor;
char NombreActor[30];
char ApellidoActor[30];
int BajaActor;
};

struct usuario {
char Nom[15];
char Contra[15];
int Perfil;
}; 

// Datos Precargados.
struct DatosPelicula pelicula [v]={{1,"Nino","2005","Nicolas","Comedia","100"},{2,"Garfield","2000","Matias","Infantil","200"}}; 

struct DatosActor actor[v]={{1,"Nicolas","Sosa"},{2,"Florencia","Conde"}},AuxActor[v];

struct usuario usu_contra[Usu]={{"admin","123",1},{"Noe","1234",2}};

// Gotoxy.
void gotoxy(short x, short y)//Para el funcionamiento del gotoxy.
{
	COORD a; 
	a.X = x;   
	a.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),a);
};

//Ordenar por nombre de Actores Alfab�ticamente .
void OrdenarPorNombreActores()
{
	int a, n, j, k; 
	
	for(n=0; n<da; n++) 	
	{ 
		k=0; 	
		// Recorre as� mismo para validar todos los nombres.
		for(j=0; j<da; j++) 	
		{ 
			//strcmp devuelve 0 si son iguales despues devuelve < 0 si es menor y > 0 si la cadena es mayor.
			a = strcmp(actor[n].NombreActor, actor[j].NombreActor); 
			if (a>0) //si la cadena es mayor.
				k=k+1; 	
		}
		
		AuxActor[k] = actor[n]; 
		
	}
}

// Validaci�n de letras.
int ValidarLetras(char cadena[50])
{
	int i=0, sw=0 ,j;
	j=strlen(cadena);////Cantidad de letras que contiene el parametro
	
	while(i<j && sw==0)
	{
		if(isalpha(cadena[i])!=0)//Si es letra retorna distinto de 0 si es numero retorna 0.
			{
				i++;
			}		
		else
		{
			sw=1;
		}
	}
	return sw;
}

// Validaci�n de n�meros.
int ValidarNumeros(char numero[50])
{
	int i=0,sn=0,j;
	
	j=strlen(numero);////Retorna el n�mero de caracteres que contenga la cadena.
	
	while(i<j && sn==0)
	{
		if(isdigit(numero[i])!=0)//Retorna distinto de 0 si es un n�mero, si es letra reetorna 0.
		{
			i++;
		}
		else
		{
			sn=1;
		}
	}
	return(sn);
}

//Validacion decimal
int ValidarDecimal(char decimal[50])
{
	int i=0,vd=0,j;
		
	j=strlen(decimal);
	
	while (i<j && vd==0)
	{
		if(decimal[i]!='.')
		{
			i++;
		}
		else
		{
			vd=1;
		}
	}
	return(vd);
}

int main()
{
	Presentacion (); 
}

int Presentacion () // Primera pantalla de presentaci�n.
{ //Inicio del m�todo Presentaci�n.
	system("color 3F"); //Color de fonde y de letra.
	printf("#########################X.   #####################\n###################+    =###;-x####################\n##############+,=###X,   -==X######################\n#############x     XXXx############################	BIENVENIDO\n########.  -###x.,+xxx##X-+##+  ###################\n#####+=Xx,   ++x#X-.X##   ###   x##################\n####X   ,##;. ###   ##X   +,    x##################	      A TU\n#####-       x##.  .,        .  .##################\n######       ,         .;        ##################\n#######        ..    .  .        -###############x;		   VIDEO CLUB\n#######;        .     ; ...   ..  #############- ,+\n########        ;.    ;;.;;  .,.  +#########=,    ;\n########    . . .     .. ,;,-,...  #######-,==  +X#\n########+  .           .   .       X###+   ,;  ####\n#########       . .               =##=;;  .+##  ;##\n#########,  .. .          .  xx###x   -,+###X##x  x\n##########          .+++##,.####x=.  ,x##XXXXxX##- \n##########+  .-x+  x==X##x ###.  +-x###XXXxXxXxXX##\n#########x-=##x+  ###X=+X ##=,    ###XXxXxXxXxxxXX#\n########,-##+=#. x###### .  ,-+##. .###XXXxXxXXX###\n######+;+xx==#; ++=X###  . .x######.  X##XXX#####=-\n####x;,##+,x#; ####+-,  =x####XXXX###,  X###Xx.  .-\n###;-##-.=##- x###=  . .####XXXxXXXX###= .+.,.  =##\n#x-+x+-=###x =X-  . ++#+..=###XXX#X####X=   +X#####\n;=X#+,x#####-.   ;X#X-###+, -X#####x-  .=+#########");
	
	 for(i=0; i<=79; i++)
    {
     Sleep(20);//Duraci�n de la pantalla.
    }
    system("cls"); //Limpia la pantalla.
	Cargando();
}//Cierre del m�todo Presentaci�n.

int Cargando() // Segunda pantalla de presentaci�n.
{	//Inicio del m�todo Cargando.
	system("color 0A"); 
    printf("\n\n\n\n\n\n\n\n\t\t\t   Cargando el Sistema...\n");
    for(i=0; i<=79; i++) 
    {
     printf("_");
    }
    for(i=0; i<=79; i++)
    {
     printf("%c",219);
     Sleep(30);
    }
    for(i=0; i<=79; i++)
    {
     Sleep(20);
    }
    Login()	; 
	system("cls");
} //Cierre del m�todo Cargando.

int Login()
{//Inicio del m�todo Login.
	perfil_usu = LoginAdmin();
	if (perfil_usu == 1)
	{
		MenuAdmin();
	}
	else
	{
	MenuUsu();
	}
			
}//Cierre del m�todo Login.

int LoginAdmin()
{//Inicio del m�todo LoginAdmin.
	system("color 70");
	char Nom [15] ; 
	char Contra [15] ;  
	int i=0;
	int x=0;
	//int y;
	//int sw;
	for(x=0;x<3;x++)
	{//Inicio del ciclo for.
		
		system ("cls") ;
		fflush(stdin); // Limpia para no traer basura.
		Contra[0]='\0'; 
		Nom[0]='\0'; 
		i = 0;
		//int x=23,y=6;
	    //gotoxy(x,y);
		printf("Ingrese usuario: ");
		fflush(stdin); // Limpia para no traer basura.
		gets(Nom);
		//y++;
		gotoxy(x,y);
		printf("Ingrese contrase%ca: ",164);
		//y++;
		
		while(Contra[i]!=13)   //Oculta la contrase�a.
    	{//Inicio del ciclo while.
        	Contra[i]=getch(); 
        	if(Contra[i]>32 && i<20) 
            { 
              putchar('*'); 
              i++; 
              
            }
			else if(Contra[i]==8 && i>0) 
            { 
              putchar(8); 
              putchar(' '); 
              putchar(8); 
              i--;  
            } 
		}//Cierre del ciclo while.
		Contra[i]='\0'; 
		for(y=0;y<Usu;y++)
		{//Inicio del ciclo for.
			if ( (strcmp(usu_contra[y].Nom,Nom)==0) && (strcmp(usu_contra[y].Contra,Contra)==0) )
			{	
				return usu_contra[y].Perfil; 
				pos_usu=i;
			}
		}//Cierre del ciclo for.
		
			if(x==2)
		{
			system("cls");
			//int x=23,y=6;
			printf("\n\n\n\n\n\t\t %c%c%cAcceso denegado!!! \n ",173,173,173);  
			printf("\n\n\n\n\n\t\t Superaste los intentos, el programa se cerrar%c... \n ",160); 
			//y++; 
			getch();//Congela la pantalla.
			exit(0);
		}
		else
		{
			system("cls"); 
			//int x=25,y=4;
	   		//gotoxy(x,y);
			printf("\n\n\n\n\n\t\t%c%c%cAcceso denegado!!!\n ",173,173,173); 
			//y++;
			getch();		
		}
		
	}//Cierre del ciclo for.
	
}//Cierre del m�todo LoginAdmin.

int MenuAdmin ()
{//Inicio del m�todo MenuAdmin.
	system("cls");
	system("color 1F");  
	int OP;
	printf("\n*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.Bienvenido al Men%c.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*\n\n",163); 
	printf("Ingrese una opci%cn:\n\n\n",162);
	printf("1-	Alta\n\n\n");
	printf("2-	Baja\n\n\n");
	printf("3-	Modificar\n\n\n");
	printf("4-	Listar\n\n\n");
	printf("5-  Buscar\n\n\n");
	printf("6-	Cerrar sesi%cn\n\n\n",162); 
	printf("7-	Salir\n\n\n");
	scanf("%d",&OP);
	switch(OP)
	{//Inicio de la sentencia switch.
		case 1 : 
				system ("cls");
				Alta();
				break;
		case 2 :
				system ("cls");
				Baja();
				break;
		case 3 :
				system ("cls");
				Modificar();
				break;
		case 4 :
				system ("cls");
				Listar();
				break;
		case 5 :
				system("cls");
				Buscar();
				break;
		case 6 :
				system("cls");
				Login();
				break;	
		case 7 : exit(0);
		
		default: exit(0);
	}//Cierre de la sentencia switch.
	
}//Cierre del m�todo MenuAdmin.

int Alta ()	
	{//Inicio del m�todo Alta.
		int op;
		system("cls");
		printf("\tMen%c Alta\n",163);
		printf("Ingrese la opci%cn deseada\n",162);
		printf("1- Alta de Peliculas\n");
		printf("2- Alta de Actores\n"); 
		scanf("%d",& op);
		if(op==1)
		{
			system("cls");
			AltaPelicula();
		}
		else 
		{
			system("cls");
			AltaActor();
		}
	
	 	
	}//Cierre del m�todo Alta.
		
int AltaPelicula ()
{//Inicio del m�todo AltaPelicula 
		int i ; i=1 ; 
		int j ; 
		int vl;
		int vn;
		int vd;
		system("cls");
		printf("\tMen%c Alta de Pelicula\n",163);
		
		do
	 {//Inicio del ciclo do-while.
		printf("El C%cdigo de la Pel%ccula es : %d \n",162,161,IdPelicula);
	 	pelicula[dp].CodigoPelicula = IdPelicula;
	 	IdPelicula++;
	 	
	 	int tp=1;
	 	while(tp==1)
	 	{
	 		printf("Ingrese el T%ctulo de la Pel%ccula:\n",161,161);
	 		scanf("%s",&pelicula[dp].Titulo);
	 	
		vl= ValidarLetras(pelicula[dp].Titulo);
			if(vl==0)
			{
				tp++; 
			}
			else
			{
				printf("Ingrese solo letras");
				getch();//devuelve un char que representa el c�digo ASCII de la tecla pulsada
			}
		}
	 	int ae=1;
	 	while(ae==1)
	 	{
	 	system("cls"); 
		printf("Ingrese el a%co de estreno:\n",164);
	 	scanf("%s",&pelicula[dp].AnioDeEstreno);
	 	
	 	vn= ValidarNumeros(pelicula[dp].AnioDeEstreno);
	 		if(vn==0)
	 		{
	 			ae++;
			}
			else
			{
				printf("Ingrese solo numeros"); 
				getch();
			}
		}
		 
	int ap=1;
	while(ap==1)
	{
		system("cls");
		printf("Ingrese el Actor Principal:\n");
	 	scanf("%s",&pelicula[dp].ActorPrincipal);
	 	
	 	vl= ValidarLetras(pelicula[dp].ActorPrincipal);
	 	if(vl==0)
	 	{
	 		ap++;
		} 
		else
		{
			printf("Ingrese solo letras"); 
			getch();
		}
	}
	 		
		int g=1;
		while(g==1)
		{
		system("cls");
		printf("Ingrese el G%cnero:\n",130);
	 	scanf("%s",&pelicula[dp].Genero);
	 	
	 	vl=ValidarLetras(pelicula[dp].Genero);
	 	if(vl==0)
	 	{
	 		g++;	
		}
		else
		{
			printf("Ingrese solo letras");
			getch();
		}
	 	
		}
		 	
	
	int p=1;
	while(p==1)		
		{
		system("cls"); 
	 	printf("Ingrese el Precio:\n");
	 	scanf("%s",&pelicula[dp].Precio);

		
			vd=ValidarDecimal(pelicula[dp].Precio);
			
			if(vd==1)
			{
				p++;
			}
			else
			{
				printf("El dato debe contener valores decimales, por ejemplo 100.00");
				getch();
				
			}
		}
	 	
	 	
	 	
	 	printf("�Desea volver a ingresar una Pel%ccula? Si=1 No=2\n",161);
	 	scanf("%d",&i);
	 	dp++;//Aumento la variable global
	 	system("cls");
	}while(i==1); //Cierre del ciclo do-while.
			system("cls");
			MenuAdmin(); 
			
 

 
}//Cierre del m�todo AltaPelicula.
	
int AltaActor ()
{	//Inicio del m�todo AltaActor.
		int f ; f=1 ; 
		int vl;
		system("cls");
		printf("\tMen%c Alta de Actor\n",163);
		do //Inicio del ciclo do-while.
	 {
	 	printf("El Codigo del Actor es : %d \n",IdActor);
	 	actor[da].CodigoDeActor = IdActor;
	 	IdActor++;
	 	
		int n=1;
		while(n==1)
		{
		system("cls");
		printf("Ingrese el Nombre del Actor:\n");
	 	scanf("%s",&actor[da].NombreActor);	
	 	vl=ValidarLetras(actor[da].NombreActor); 
	 	if(vl==0)
	 	{
	 		n++;
		}
		else
		{
			printf("Ingrese solo letras");
			getch();
		}
		}
		
		int a=1;
		while(a==1) 
		{
		system("cls");
		printf("Ingrese el Apellido del Actor:\n");
	 	scanf("%s",&actor[da].ApellidoActor);
	 	vl=ValidarLetras(actor[da].ApellidoActor);
	 	if(vl==0)
	 	{
	 		a++;
		}
		else
		{
			printf("Ingrese solo letras");
			getch();
		}
	 	
		}


	 	printf("�Desea volver a ingresar un Actor? Si=1 No=2\n");
	 	scanf("%d",&f);
	 	da++;//Aumenta la variable local
	 	system("cls");
	 }while (f==1); //Cierre del ciclo do-while.
		
			system("cls");
			MenuAdmin();
		
		
}//Cierre del m�todo AltaActor.
	
int Modificar ()	
	{//Inicio del m�todo Modificar.
		int op;
		system("cls");
		printf("\tModificaciones\n");
		printf("Ingrese la opci%cn deseada\n",162);
		printf("1- Modificar Pel%cculas\n",161);
		printf("2- Modificar Actores\n"); 
		scanf("%d",& op);
		if(op==1)
		{
			system("cls");
			ModificarPelicula();
		}
		else 
		{
			system("cls");
			ModificarActor();
		}
	}//Cierre del m�todo Modificar.
	
int ModificarPelicula()
	{//Inicio del m�todo ModificarPelicula.
	int cod;
	int i;
	int modp;
	int cont=0 ;
	int aux;
	system("cls");	
	printf("\tModificar Pel%cculas\n",161);
	printf("Ingrese c%cdigo a buscar:\n",162);
	scanf("%d",&cod); 
	
	for(i=0;i<dp;i++)
	{//Inicio del ciclo for.
		if(cod==pelicula[i].CodigoPelicula)
		{//Inicio de la instrucci�n if.
			aux=i;
	 
			if (pelicula[aux].BajaPelicula==0) 
			{//Inicio de la instrucci�n if anidada.
			cont++; 
			printf("El c%cdigo de pel%ccula es : %d\n ",162,161,pelicula[aux].CodigoPelicula);
			printf("El t%ctulo de la pel%ccula es :  %s\n",161,161,pelicula[aux].Titulo);
			printf("El a%co de estreno de la pel%ccula es :  %d\n",164,161,pelicula[aux].AnioDeEstreno);
			printf("El Actor principal de la pel%ccula es : %s\n",161,pelicula[aux].ActorPrincipal);
			printf("El g%cnero de la pel%ccula es : %s\n",130,161,pelicula[aux].Genero);
			printf("El precio de la pel%ccula es : %f\n",161,pelicula[aux].Precio); 
			printf("Ingrese una opci%cn:\n",16162); 
			printf("1-Modificar el t%ctulo\n",161);
			printf("2-Modificar el a%co de estreno\n",164);
			printf("3-Modificar el Actor principal\n"); 
			printf("4-Modificar el g%cnero\n",130);
			printf("5-Modificar el precio\n");
			printf("6-Modificar todo\n");
			printf("7-Volver al men%c\n",163);
			scanf("%d",&modp); 
			system("cls");
			
		switch (modp)
		{//Inicio de la sentencia switch.

				
			case 1:
				{
				int op; op=1;
				while(op==1)
				{
				printf("Ingrese el nuevo t%ctulo de la Pel%ccula:\n",161,161);
				scanf("%s",&pelicula[aux].Titulo);
				printf("�Desea modificar otro dato 1-Si 2-No?");
				scanf("%d",&op); 
				if(op==1)
				{
					Modificar();
				}
				else
				{
					MenuAdmin();
				}
				}	
				system("cls");
				break;
			}
			case 2:
				{
				int op; op=1;
				while(op==1)
				{
				printf("Ingrese el nuevo a%co de estreno de la Pel%ccula:\n",164,161);
	 			scanf("%d",&pelicula[aux].AnioDeEstreno);
				printf("�Desea modificar otro dato 1-Si 2-No?");
				scanf("%d",&op); 
				if(op==1)
				{
					Modificar();
				}
				else
				{
					MenuAdmin();
				}
				}	
				system("cls");
				break;
			}
			case 3:
				{
				int op; op=1;
				while(op==1)
				{
				printf("Ingrese el nuevo Actor principal de la Pel%ccula:\n",161);
	 			scanf("%s",&pelicula[aux].ActorPrincipal);
	 			printf("�Desea Modificar otro dato 1-Si 2-No?");
				scanf("%d",&op); 
				if(op==1)
				{
					Modificar();
				}
				else
				{
					MenuAdmin();
				}
				}	
				system("cls");
				break;
			}
			case 4:
				{
				int op; op=1;
				while(op==1)
				{
				printf("Ingrese el nuevo g%cnero de la Pel%ccula:\n",130,161);
	 			scanf("%s",&pelicula[aux].Genero);
	 			printf("�Desea Modificar otro dato 1-Si 2-No?");
				scanf("%d",&op); 
				if(op==1)
				{
					Modificar();
				}
				else
				{
					MenuAdmin();
				}
				}	
				system("cls");
				break;
			}
			case 5:
				{
				int op; op=1;
				while(op==1)
				{
				printf("Ingrese el nuevo precio de la Pel%ccula:\n",161);
	 			scanf("%f",&pelicula[aux].Precio);
				printf("�Desea modificar otro dato 1-Si 2-No?");
				scanf("%d",&op); 
				if(op==1)
				{
					Modificar();
				}
				else
				{
					MenuAdmin();
				}
				}	
				system("cls");
				break;
			}
			case 6:
				{
				int op; op=1;
				while(op==1)
				{
				printf("Ingrese el nuevo c%cdigo de la Pel%ccula:\n",162,161);
				scanf("%d",&pelicula[aux].CodigoPelicula);
				printf("Ingrese el nuevo t%ctulo de la Pel%ccula:\n",161,161);
				scanf("%s",&pelicula[aux].Titulo);
				printf("Ingrese el nuevo a%co de estreno de la Pel%ccula:\n",164,161);
	 			scanf("%d",&pelicula[aux].AnioDeEstreno);
	 			printf("Ingrese el nuevo Actor principal de la Pel%ccula:\n",161);
	 			scanf("%s",&pelicula[aux].ActorPrincipal);
	 			printf("Ingrese el nuevo g%cnero de la Pelicula:\n",130,161);
	 			scanf("%s",&pelicula[aux].Genero);
	 			printf("Ingrese el nuevo precio de la Pel%ccula:\n",161);
	 			scanf("%f",&pelicula[aux].Precio);
	 			printf("�Desea Modificar otro dato 1-Si 2-No?");
				scanf("%d",&op); 
				if(op==1)
				{
					Modificar();  
				}
				else
				{
					MenuAdmin();
				}
				}	
				system("cls");  
				break;
			}
	 		case 8:
	 			MenuAdmin();
	 			break;
	 		default: exit(0);  
	 		}//Cierre de la sentencia switch.
	 	} //Cierre de la instrucci�n if anidada.
	}//Cierre de la instrucci�n if.
		
 }	//Cierre del ciclo for.
	
	 	if(cont==0)	
			{
				printf("%cC%cdigo de Pel%ccula incorrecto!",173,162,161);
				getch();
				MenuAdmin();
			}
       
} //Cierre del m�todo ModificarPelicula.
	
	
int ModificarActor()
{//Inicio del m�todo ModificarActor.
	int cod;
	int i; 
	int moda;
	int cont=0;
	int aux;
	system("cls");	
	printf("\tModificar Actores\n");
	printf("Ingrese c%cdigo a buscar:\n",162);
	scanf("%d",&cod); 

	for(i=0;i<da;i++)
	{//Inicio del ciclo for.
		if(cod==actor[i].CodigoDeActor)
		{//Inicio de la instrucci�n if.
			aux=i;
			if (pelicula[aux].BajaPelicula==0) 
		{//Inicio de la instrucci�n if anidada.
			cont++;
			printf("El c%cdigo de Actor es : %d\n ",162,actor[aux].CodigoDeActor);
			printf("El Nombre del Actor es : %s\n",actor[aux].NombreActor);
			printf("El Apellido del Actor es : %s\n",actor[aux].ApellidoActor);
			printf("Ingrese una opci%cn:\n",162); 
			printf("1-Modificar el c%cdigo \n",162); 
			printf("2-Modificar el Nombre\n");
			printf("3-Modificar el Apellido\n");
			printf("4-Modificar todo\n");
			printf("5-Volver al men%c\n",163);
			scanf("%d",&moda); 
			system("cls");
			
			switch (moda)
			{//Inicio de la sentencia switch.
				
			case 1 :
				{
				int op; op=1;
			while(op==1)
			{//Inicio del ciclo.
				printf("Ingrese el nuevo c%cdigo de Actor:\n",162);
				scanf("%d",&actor[aux].CodigoDeActor);
				printf("�Desea Modificar otro dato 1-Si 2-No?");
				scanf("%d",&op);  
				if(op==1)
				{
					ModificarActor();
				}
				else
				{
					MenuAdmin();
				}
			}//Cierre del ciclo.
				
				system ("cls");
				break;
				}
				
			case 2:
				{
				int op; op=1;
				while(op==1)
				{
				printf("Ingrese el nuevo Nombre de Actor:\n");
				scanf("%s",&actor[aux].NombreActor);
				printf("�Desea Modificar otro dato 1-Si 2-No?");
				scanf("%d",&op); 
				if(op==1)
				{
					ModificarActor();
				}
				else
				{
					MenuAdmin();
				}
				}	
				
				system ("cls");
				break;
				}
			case 3:
				{
				int op; op=1;
				while(op==1)
				{
				printf("Ingrese el nuevo Apellido de Actor:\n");
	 			scanf("%d",&actor[aux].ApellidoActor);
				printf("�Desea Modificar otro dato 1-Si 2-No?");
				scanf("%d",&op); 
				if(op==1)
				{
					Modificar();
				}
				else
				{
					MenuAdmin();
				}
				}	
				system("cls");
				break;
			}
			case 4:	
				{
				int op; op=1;
				while(op==1)
				{
				printf("Ingrese el nuevo c%cdigo de Actor:\n",162);
				scanf("%d",&actor[aux].CodigoDeActor);
				printf("Ingrese el nuevo Nombre de Actor:\n");
				scanf("%s",&actor[aux].NombreActor);
				printf("Ingrese el nuevo Apellido de Actor:\n");
	 			scanf("%d",&actor[aux].ApellidoActor);
	 			printf("�Desea Modificar otro dato 1-Si 2-No?");
				scanf("%d",&op); 
				if(op==1)
				{
					Modificar();
				}
				else
				{
					MenuAdmin();
				}
				}	
				system("cls");
				break;
			}
	 		case 5:
	 	
	 				MenuAdmin();
	 			break;
	 		default: exit(0);  
				 
	 			
	 		}//Cierre de la sentencia switch.
		  }//Cierre de la instrucci�n if anidada.
		}//Inicio de la instrucci�n if.
	}//Cierre del ciclo for.
				if(cont==0)	
			{
				printf("%cC%cdigo incorrecto!",173,162);
				getch();
				MenuAdmin();
			}	
			
} //Cierre del m�todo ModificarActor.
 
int Baja ()	
	{//Inicio del m�todo Baja.
		int op;
		system("cls");
		printf("\tMen%c Baja\n",163);
		printf("Ingrese la opci%cn deseada\n",162);
		printf("1- Baja de Pel%cculas\n",161);
		printf("2- Baja de Actores\n"); 
		scanf("%d",& op); 
		if(op==1)
		{
			system("cls");
			BajaPelicula();
		}
		else 
		{
			system("cls");
			BajaActor();
		} 	
	}//Cierre del m�todo Baja.
	
int BajaPelicula()
{//Inicio del m�todo BajaPelicula.		
	int cod;
	int b;
	int modp;
	int cont=0 ;
	int bk=0;
	int op;
	int o=1;
	
	int aux;
	system("cls");	
	printf("\tBaja de Pel%cculas\n",161);
	printf("Ingrese c%cdigo a buscar:\n",162);
	scanf("%d",&cod); 
	for(b=0;b<dp;b++) 
	{//Inicio del ciclo for.
	
	
	if (pelicula[o].BajaPelicula==0) 
	{
				if(cod==pelicula[b].CodigoPelicula) 
		{
			aux=b;
			bk = 1;
	    }
	    
	}
	

	}//Cierre del ciclo for. 
	
	
	if(bk==1)
	{
	    if (pelicula[aux].BajaPelicula==0)   
	    {//Inicio de la instrucci�n if.
	    	cont++;
			printf("El c%cdigo de Pel%ccula es : %d\n ",162,161,pelicula[aux].CodigoPelicula);
			printf("El t%ctulo de la Pel%ccula es : %s\n",161,161,pelicula[aux].Titulo);
			printf("El a%co de estreno de la Pel%ccula es : %d\n",164,161,pelicula[aux].AnioDeEstreno);
			printf("El Actor principal de la Pel%ccula es : %s\n",161,pelicula[aux].ActorPrincipal);
			printf("El g%cnero de la Pel%ccula es: %s\n",130,161,pelicula[aux].Genero);
			printf("El precio de la Pel%ccula es : %f\n",161,pelicula[aux].Precio); 
			printf("�Desea dar de Baja esta Pel%ccula? 1-Si 2-No\n",161);
			scanf("%d",&op);
	
		
	
			if(op==1)
			{//Inicio de la instrucci�n if anidada.
				pelicula[aux].BajaPelicula=1;
				printf("Baja realizada correctamente"); 
				getch();
				MenuAdmin();
			}//Cierre de la instrucci�n if anidada.
		
			if(op==2)
			{
				system("cls");
				MenuAdmin();
			}
			if(cont==0)
			{
				printf("%cC%cdigo no encontrado!",173,162);
				getch();
				MenuAdmin();
			}
		}//Cierre de la instrucci�n if.
	}

	
	if(bk==0)  
	{
	
		printf("%cC%cdigo no encontrado!",173,162);
		getch();
		MenuAdmin();
	}


}//Cierre del m�todo BajaPelicula.
		
int BajaActor() 
{//Inicio del m�todo BajaActor.			
	int cod;
	int b;
	int modp;
	int cont=0 ;
	int op;
	int o=1;
	int aux;
	int bk=0;
	system("cls");	
	printf("\tBaja de Actores\n");
	printf("Ingrese c%cdigo a buscar:\n",162);
	scanf("%d",&cod); 
	for(b=0;b<dp;b++)
	{//Inicio del ciclo for.
	
	if (actor[o].BajaActor==0) 
	{
		if(cod==actor[b].CodigoDeActor) 
		{
			aux=b;
			bk = 1;
	    }
	}
	

	}//Cierre del ciclo for. 
	     
	if(bk==1)
	
	{
	
	     if (actor[aux].BajaActor==0)   
		
	    {//Inicio de la instrucci�n if.
	    	cont++;
			printf("El c%cdigo de Actor es : %d\n ",162,actor[aux].CodigoDeActor);
			printf("El Nombre del Actor es : %s\n",actor[aux].NombreActor);
			printf("El Apellido del Actor es : %s\n",actor[aux].ApellidoActor);
			printf("�Desea dar de Baja esta Pel%ccula? 1-Si 2-No\n",161);
			scanf("%d",&op);
	
		
	
		if(op==1)
		{//Inicio de la instrucci�n if anidada.
			actor[aux].BajaActor=1;
			printf("Baja realizada correctamente"); 
			getch();
			MenuAdmin();
		}//Cierre de la instrucci�n if anidada.
	}//Cierre de la instrucci�n if.
  
	if(op==2)
		{
			system("cls");
			MenuAdmin();
		}
		if(cont==0)
		{
			printf("%cC%cdigo incorrecto!",173,162);
			getch();
			MenuAdmin(); 
		}
	
	
	}
	
		if(bk==0)  
	{
	
		printf("%cC%cdigo no encontrado!",173,162);
		getch();
		MenuAdmin();
	}

	
}//Cierre del m�todo BajaActor.		
	

int Listar()
{//Inicio del m�todo Listar.	
	int op;
		system("cls");
		printf("\tMen%c Listar\n",163);
		printf("Ingrese la opci%cn deseada\n",162);
		printf("1- Listado de Pel%cculas\n",161);
		printf("2- Listado de Actores\n"); 
		scanf("%d",& op); 
		if(op==1)
		{
			system("cls");
			ListarPelicula();
		}
		else 
		{
			system("cls");
			ListarActor(); 
		}
}//Cierre del m�todo Listar.	
	
int ListarPelicula ()	
{//Inicio del m�todo ListarPelicula.	
	int n;
	system("cls");	
	printf("\tListado de Pel%cculas\n",161);
	for(n=0;n<dp;n++)
	{//Inicio del ciclo for.
		if (pelicula[n].BajaPelicula==0)  
		{//Inicio de la instruccion if.
			printf("El c%cdigo de Pel%ccula es : %d\n ",162,161,pelicula[n].CodigoPelicula);
			printf("El t%ctulo de la Pel%ccula es : %s\n",161,161,pelicula[n].Titulo);
			printf("El a%co de estreno de la Pel%ccula es : %d\n",164,161,pelicula[n].AnioDeEstreno);
			printf("El Actor principal de la Pel%ccula es : %s\n",161,pelicula[n].ActorPrincipal);
			printf("El g%cnero de la Pel%ccula es: %s\n",130,161,pelicula[n].Genero);
			printf("El precio de la Pel%ccula es : %f\n\n\n",161,pelicula[n].Precio); 
		}//Cierre de la instruccion if.
	}//Cierre del ciclo for.
	getch();
	system("cls");
	MenuAdmin();
}//Inicio del m�todo ListarPelicula.

int ListarActor() 
{//Inicio del m�todo ListarActor.
	int n;
	system("cls");	
	printf("\tListado de Actores\n");
	OrdenarPorNombreActores();
	for(n=0;n<da;n++) 
	{//Inicio del ciclo for.
		if (actor[n].BajaActor==0)  
		{//Inicio de la instrucci�n if.
			printf("El c%cdigo de Actor es : %d\n ",162,actor[n].CodigoDeActor);
			printf("El Nombre del Actor es : %s\n",actor[n].NombreActor);
			printf("El Apellido del Actor es : %s\n\n\n",actor[n].ApellidoActor);
		}//Cierre de la instrucci�n if.
	}//Cierre del ciclo for.
	getch();
	system("cls");
	MenuAdmin();
}//Cierre del m�todo ListarActor.

int Buscar()
{
	int OP;
	printf("Bienvenidos al buscador\n\n\n");
	printf("Ingrese una opci%cn:\n\n\n",162);
	printf("1- Buscar pel%cculas\n\n\n",161);
	printf("2- Buscar actores\n\n\n");
	scanf("%d",&OP);
		if(OP==1)
		{
			system("cls");
			BuscarPelicula();
		}
		else 
		{
			system("cls");
			BuscarActor();
		}
	
}

int BuscarPelicula()
{
	system("cls");
	int OP;
	printf("Bienvenidos al buscador de pel%cculas\n\n\n",161);
	printf("Ingrese una opci%cn:\n\n\n",162);
	printf("1-Buscar pel%cculas por c%cdigo\n\n\n",161,162);
	printf("2-Buscar pel%cculas por t%ctulo\n\n\n",161,161);
	printf("3-Buscar pel%cculas por a%co de estreno\n\n\n",161,164);
	printf("4-Buscar pel%cculas por actor principal\n\n\n",161);
	printf("5-Buscar pel%cculas por g%cnero\n\n\n",161,130);
	printf("6-Buscar pel%cculas por precio\n\n\n",161);
	printf("7-Volver");
	scanf("%d",&OP);
	switch(OP)
	{//Inicio de la sentencia switch.
		case 1 : 
				system ("cls");
				BuscarPeliculaCodigo(); 
				break;
		case 2 :
				system ("cls");
				BuscarPeliculaTitulo();
				break;
		case 3 :
				system ("cls");
				BuscarPeliculaAnio();
				break;
		case 4 :
				system ("cls");
				BuscarPeliculaActorP();
				break;
		case 5 :
				system("cls");
				BuscarPeliculaGenero();
				break;
		case 6 :
				system("cls");
				BuscarPeliculaPrecio();
				break;
		case 7 :
				system("cls");
				int Buscar();
				break;	
		case 8 : exit(0);
		
		default: exit(0);
	}//Cierre de la sentencia switch.

}

int BuscarPeliculaCodigo() 
{
	system("cls");
	printf("Bienvenidos al buscardor de pel%cculas por c%cdigo\n\n\n",161,162);
}

int BuscarPeliculaTitulo()
{
		system("cls");	
		printf("Bienvenidos al buscardor de pel%cculas por t%ctulo\n\n\n",161,161);
}

int BuscarPeliculaAnio()
{
	system("cls");
	printf("Bienvenidos al buscardor de pel%cculas por a%co de estreno\n\n\n",161,164);
}

int BuscarPeliculaActorP()
{
	system("cls");	
	printf("Bienvenidos al buscardor de pel%cculas por actor principal\n\n\n",161);
}

int BuscarPeliculaGenero()
{
	system("cls");
	printf("Bienvenidos al buscardor de pel%cculas por g%cnero\n\n\n",161,130);
}

int BuscarPeliculaPrecio()
{
	system("cls");
	printf("Bienvenidos al buscardor de pel%cculas por precio\n\n\n",161);
}

int BuscarActor()
{
	
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int MenuUsu()
{	
	system("cls");
	system("color 1F"); 
	int OP;
	printf("\n*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*.Bienvenido al Men%c.*.*.*.*.*.*.*.*.*.*.*.*.*.*.*\n\n",163); 
	printf("Ingrese una opci%cn:\n\n\n",162);
	printf("1-	Listar\n\n\n");
	printf("2-	Buscar\n\n\n");
	printf("3-	Cerrar sesi%cn\n\n\n",162);
	printf("4-	Salir\n\n\n");
	scanf("%d",&OP);
	switch(OP)//
	{
		case 1 : 
				system ("cls");
				ListarUsu();
				break;
		case 2 :
				system ("cls");
				BuscarUsu();
				break;
		case 3 :
				system ("cls");
				Login();
				break;
		case 4 :
				exit(0);
		
		default: exit(0);
	}
}

int ListarUsu()
{//Inicio del m�todo ListarUsu.
int op;
		system("cls");
		printf("\tMen%c Listar\n",163);
		printf("Ingrese la opci%cn deseada\n",162);
		printf("1- Listado de Pel%cculas\n",161);
		printf("2- Listado de Actores\n"); 
		scanf("%d",& op); 
		if(op==1)
		{
			system("cls");
			ListarPeliculaUsu();
		}
		else 
		{
			system("cls");
			ListarActorUsu();
		}		
}//Cierre del m�todo ListarUsu.

int ListarPeliculaUsu()
{//Inicio del m�todo ListarPeliculaUsu.
	int n;
	system("cls");	
	printf("\tListado de Pel%cculas\n",161);
	for(n=0;n<dp;n++)
	{//Inicio del ciclo for.
		if (pelicula[n].BajaPelicula==0)  
		{//Inicio de la instruccion if.
			printf("El c%cdigo de Pel%ccula es : %d\n ",162,161,pelicula[n].CodigoPelicula);
			printf("El t%ctulo de la Pel%ccula es : %s\n",161,161,pelicula[n].Titulo);
			printf("El a%co de estreno de la Pel%ccula es : %d\n",164,161,pelicula[n].AnioDeEstreno);
			printf("El Actor principal de la Pel%ccula es : %s\n",161,pelicula[n].ActorPrincipal);
			printf("El g%cnero de la Pel%ccula es: %s\n",130,161,pelicula[n].Genero);
			printf("El precio de la Pel%ccula es : %f\n\n\n",161,pelicula[n].Precio); 
		}//Cierre de la instruccion if.
	}//Cierre del ciclo for.
	getch();
	system("cls");
	MenuUsu();
}//Cierre del m�todo ListarPeliculaUsu.

int ListarActorUsu()
{

	int n;
	system("cls");	
	printf("\tListado de Actores\n");
	for(n=0;n<dp;n++)
	{//Inicio del ciclo for.
		if (actor[n].BajaActor==0)  
		{//Inicio de la instrucci�n if.
			printf("El c%cdigo de Actor es : %d\n ",162,actor[n].CodigoDeActor);
			printf("El Nombre del Actor es : %s\n",actor[n].NombreActor);
			printf("El Apellido del Actor es : %s\n\n\n",actor[n].ApellidoActor);
		}//Cierre de la instrucci�n if.
	}//Cierre del ciclo for.
	getch();
	system("cls");
	MenuUsu();
}

int BuscarUsu()
{
	system("cls");
	printf("Buscar");
}
	

	
