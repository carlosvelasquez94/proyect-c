//	Declaracion de librerias

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#include <windows.h>

/* Una veterinaria les encarga el desarrollo de un programa  para llevar el control de sus pacientes (mascotas), los datos de los due�os.
De acuerdo a las siguientes consideraciones:
1.	Datos de las mascotas. (c�digo de mascota, nombre, fecha de nacimiento, tipo, raza, due�o)
2.	Datos de los due�os. (c�digo, apellido, nombres, tipo, nro. de documento, tel�fono, direcci�n)


Se pide realizar:

1.	Men� de acceso a la aplicaci�n,(1)
2.	ABM de las mascotas, due�os. (1)
3.	Listados de cada uno(1),  ordenados alfab�ticamente.(2)
4.	B�squedas a trav�s de diferentes datos de mascotas, due�os .(1)
5.	Calcular y mostrar la edad de las mascotas, en un listado ordenado por edad en forma ascendente. (2)
6.	Determinar e informar los c�digos y nombres  de las mascotas,  y en el siguiente rengl�n los servicios que recibi�. (3)
*/

void gotoxy(short x, short y) //para que funcione el gotoxy
{
	COORD a;
	a.X = x;
	a.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),a);
};



struct mascota	{  		  // Estructuras de datos de mascota...
	char cod[20];
	char nombre [10];
	char dia[20];
	char mes [10];
	int anio;
	char tipo [20];
	char raza [16];
	char duenio[20];
	char servicio[60];
	int edad;
	int bajlog;
	
};

struct duenio	{         // estructuras de datos del due�o
	char codigo[20];
	char nombre[10];
	char apellido [20];
	char tipo [20];
	char documento [10];
	char telefono[10];
	char direccion [20];
	int bajlog;
	};

// PRECARGAS Y DECLARACION DE VARIABLES

struct mascota animales [100]= {{"1","Nupi","1","Marzo",2000,"Perro", "Beagle", "Juan","Desparacitacion",15,0},{"2","Pepe","23","Febrero",2009,"Gato","Siames","Jose","Castracion",6,0},{"3","Pupi","12","Agosto",2011,"Hamster","Dorado","Maria","Vacuna",4,0}},aux2[100];  // precarga datos de animales
struct duenio persona[100]={{"1","Juan","Velez","DNI","23892929","46018519","La Paz",0},{"2","Jose","Cardozo","DNI","32475921","46821564","Olaguer",0},{"3","Maria","Diaz","DNI","40586723","39875647","Palacios",0}},aux; // precarga datos del due�o

int alta_duenio (void);
int alta_animal(void);
int menu_modificaciones(void);
int menu_baja(void);
int baja_duenio_ap(void);
int baja_duenio_cod(void);
int baja_animal_cod(void);
int baja_animal_nom(void);
int menu_busqueda(void);
int menu_alta(void);
int lis_du(void);
int lista_animal(void);
int lista_servicio(void);
int bus_duenio_cod(void);
int bus_duenio_doc(void);
int bus_duenio_ap(void);
int bus_animal_nom(void);
int bus_animal_cod(void);
int modificar_duenio(void);
int modificar_animal(void);
int menu_lista(void);
int mp=0;

int ordenar_animal(void);

int validar_cadena(char cadena[30]);
int validar_num(char numero[30]);

int Menuprincipal(void);
int Usuario(void);
int Admin(void);
int tapa(void);

/*int ordenar(void);*/
int cuadro(void);
int o;  // variable para la opcion de busquedas y modificaciones...
int cp=3;					// variable para que no pise la precarga de animales
int cd=3;				// variable para que no pise la precarga de personas
int due;			
int ani;
int tipo=0;
int contador=0;
//----------------------------------------------------------------------------------------------------------------------------------

int Menuprincipal() // MENU PRINCIPAL - ELIGE SI ES USUARIO O ADMINISTRADOR
{
	
      system("color 1F");
      int i, j, x, y;
      int opc;
      char usuario[20];
	  char pass[5];
	
cuadro();
//OPCIONES DEL MENU PRINCIPAL - EN EL SWITCH LAS CONTRASE�AS.
    gotoxy(30,4);
      printf("Men\xA3"" Principal\n");
    gotoxy(30,5);
      printf("---- ---------\n \n \n");
    gotoxy(21,8);
      printf("1 - Perfil Usuario \n");
    gotoxy(21,9);
      printf("2 - Perfil Administrador \n");
    gotoxy(21,10);
      printf("3 - Salir del programa \n");
    gotoxy(21,13);
      printf("Elija opci\xA2""n: ");

    while (opc!=3)
    {
     fflush(stdin);
     scanf("%d", &opc);

          switch(opc)
            {
            case 1:
            	mp=1;
                 Usuario();
                 break;
            case 2:
            	gotoxy(21,14);
		printf("Ingrese usuario\n");
		gotoxy(21,15);
		scanf("%s",usuario);
		gotoxy(21,17);
		printf("Ingrese contrase%ca\n",164);
	
		gotoxy(21,18);
		scanf("%s",pass);
			if(strcmp(usuario,"admin")==0 && strcmp(pass,"1234")==0)
			{
			system("cls");
                	Admin();
                	}
				else
				{
				gotoxy(21,19);
				printf("Error de Usuario y/o contrase%ca\n",164);
				gotoxy(21,20);
				printf("Vuelva a intentar.\n");
				contador++;
				gotoxy(21,21);
				system ("pause");
				system("cls");
				if(contador==3)
				{
			printf("Intentos fallidos. Fin del programa.");
			exit(0);
		}
		Menuprincipal();
		}
                break;
            case 3:
                system("cls");
                printf("Fin del programa\n");
                exit (0);
            default:
                gotoxy(21,21);
                printf("Opci\xA2""n incorrecta. \n");
                gotoxy(21,22);
                printf("\t Por favor, elija nuevamente: ");
    			fflush(stdin);
                break;
            }
    }
}// CIERRE DEL MENU PRINCIPAL
//----------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------
int Usuario() // MENUS PRINCIPAL DE USUARIOS
{
      system("color 4F");
      int i, x, y;
      int opc;
      system("cls");
	  tipo=1;
    cuadro();
    gotoxy(30,4);
      printf("Men\xA3"" Usuario\n");
    gotoxy(30,5);
      printf("---- -------\n\n\n");
     gotoxy(20,8);
      printf("1 - Men\xA3"" Lista \n");
    gotoxy(20,9);
      printf("2 - Men\xA3"" B\xA3""squeda \n");
    gotoxy(20,10);
      printf("3 - Men\xA3"" anterior \n");
    gotoxy(20,11);
      printf("4 - Salir del programa \n");
    gotoxy(20,14);
      printf("Elija una opci\xA2""n: ");
		scanf("%d", &opc);

    while (opc!=5)
    {
          switch(opc)
            {
            case 1:
            	 system("cls");
            	 menu_lista();
            	 break;
            case 2:
                 system("cls");
                 menu_busqueda();
                 break;
            case 3:
            	 system("cls");
            	 Menuprincipal();
            	 break;
            case 4:
                 system("cls");
                 printf("Fin del programa\n");
                 exit(0);
            default:
                 gotoxy(20,15);
                 printf("Opci\xA2""n incorrecta. \n");
                 gotoxy(20,16);
                 printf("\t Por favor, elija nuevamente: ");
    			 fflush(stdin);
                 break;
            }
    }

} // FIN DEL MENUS PRINCIPAL DE USUARIOS
//----------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------
int Admin() // INICIO DEL MENUS PRINCIPAL DE ADMINISTRADORES
{
      system("color 3F");
      int i, x, y, opc=9;
      system("cls");
      tipo=2;
      mp=0;
      cuadro();
    gotoxy(30,4);
      printf("Men\xA3"" Administrador\n");
    gotoxy(30,5);
      printf("---- -------------\n\n\n");
    gotoxy(20,8);
      printf("\t 1 - Altas \n");
    gotoxy(20,9);
      printf("\t 2 - Bajas \n");
    gotoxy(20,10);
      printf("\t 3 - Modificaciones \n");
    gotoxy(20,11);
      printf("\t 4 - Listados \n");
    gotoxy(20,12);
      printf("\t 5 - B\xA3""squeda \n");
    gotoxy(20,13);
       printf("\t 6 - Men\xA3"" anterior \n");
    gotoxy(20,14);
      printf("\t 7 - Salir del programa \n");
    gotoxy(20,15);
      printf("Elija opci\xA2""n: ");

    while (opc!=5)
    {
     scanf("%d", &opc);
          switch(opc)
            {
            case 1:
            	system("cls");
                 menu_alta();
                 break;
            case 2:
            		system("cls");
                 menu_baja ();
                 break;
            case 3:
            		system("cls");
                 menu_modificaciones();
            case 4:
            		system("cls");
                 menu_lista();
            case 5:
            		system("cls");
                 menu_busqueda ();
            case 6:
                 system("cls");
                 Menuprincipal ();
            case 7:
                 system("cls");
                 printf("Fin del programa\n");
                 exit(0);
            default:
                 gotoxy(10,19);
                 printf("Opci\xA2""n incorrecta. \n");
                 gotoxy(10,20);
                 printf("\t Por favor, elija nuevamente: ");
    			 fflush(stdin);
				 break;
            }
    }
} // FIN DEL MENUS PRINCIPAL DE ADMINISTRADORES
//----------------------------------------------------------------------------------------------------------------------------------
main () //INICIO DEL  **MAIN**
{ 
int j,qq,resul;
tapa();
	
	for (j=0;j<cd-1;j++)
	{	
		for (qq=j+cd;qq<cd;qq++)
		{
		 	resul=strcmp(persona[j].apellido,persona[qq].apellido);
		
			if(resul==1)

			{
				strcpy(aux.apellido,persona[j].apellido);
  			  	strcpy(persona[j].apellido,persona[qq].apellido);
 			   	strcpy(persona[qq].apellido,aux.apellido);
     	
		     	strcpy(aux.nombre,persona[j].nombre);
    			strcpy(persona[j].nombre,persona[qq].nombre);
    			strcpy(persona[qq].nombre,aux.nombre);	
     
    			strcpy(aux.codigo,persona[j].codigo);
    			strcpy(persona[j].codigo,persona[qq].codigo);
    			strcpy(persona[qq].codigo,aux.codigo);    	
    	
    			strcpy(aux.tipo,persona[j].tipo);
    			strcpy(persona[j].tipo,persona[qq].tipo);
    			strcpy(persona[qq].tipo,aux.tipo);   	
    	
    			strcpy(aux.documento,persona[j].documento);
    			strcpy(persona[j].documento,persona[qq].documento);
    			strcpy(persona[qq].documento,aux.documento); 
    			
    			strcpy(aux.telefono,persona[j].telefono);
    			strcpy(persona[j].telefono,persona[qq].telefono);
    			strcpy(persona[qq].telefono,aux.telefono);
    			
    			strcpy(aux.direccion,persona[j].direccion);
    			strcpy(persona[j].direccion,persona[qq].direccion);
    			strcpy(persona[qq].direccion,aux.direccion);
	        }
  	    }
 	}

}

int menu_lista()
{
	cuadro();
    gotoxy(30,2);
    printf ("Men\xA3"" Listas\n");
	gotoxy(30,3);
    printf("---- ------\n\n\n");
    gotoxy(20,5);
	printf("1-Lista due\xA4""o\n");
	gotoxy(20,6);
	printf("2-Lista animales\n");
	gotoxy(20,7);
	printf("3-Lista servicios\n");
	gotoxy(20,8);
	printf("4-Volver\n");
	gotoxy(20,9);
	scanf("%d",&o);
	switch(o)
	{
		case 1:
			system("cls");
			lis_du();
			break;
		case 2:
			system("cls");
			lista_animal();
			break;
		case 3:
			system("cls");
			lista_servicio();
			break;
		case 4:
			system("cls");
			if(mp==0)
			{
			Admin();
			break;
			}
			else
			{
			Usuario();
			break;
			}
		default:
			printf("Error de ingreso");
			if(mp==0)
			{
			getch();
			Admin();
			}
			else
			{
			getch();
			Usuario();
			}
	}
}

int tapa()
{
	int qwe=1;
	int i;
	int o;
	int q=2;
	int w=2;
	int t=14;
	int g=12;
	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
	for(i=0;i<76;i++)
	{
	gotoxy(w,1);
	printf("*");
	gotoxy(w,23);
	printf("*");
	w++;
	}
	w=3;
	for(i=0;i<22;i++)
	{
	gotoxy(2,q);
	printf("�");
	gotoxy(77,q);
	printf("�");
	q++;
	}
	q=3;
	for(i=0;i<74;i++)
	{
	gotoxy(w,2);
	printf("-");
	gotoxy(w,22);
	printf("-");
	w++;
	}
	for(i=0;i<19;i++)
	{
	gotoxy(3,q);
	printf("!");
	gotoxy(76,q);
	printf("!");
	q++;
	}
	q=3;
	
	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),15);
	gotoxy(35,4);
	printf("VETERINARIA");
	gotoxy(34,5);
	printf("Happy Friends");
	gotoxy(63,18);
	printf("GRUPO 5");
	
	gotoxy(54,20);
	printf("Cristian Dias");
	gotoxy(54,21);
	printf("Cristian Provenzano");
	
	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),12);
	gotoxy(36,8);
	printf("��������");
	gotoxy(36,9);
	printf("��������");
	gotoxy(36,10);
	printf("��������");
	gotoxy(36,11);
	printf("��������");
	gotoxy(28,12);
	printf("������������������������");
	gotoxy(28,13);
	printf("������������������������");
	gotoxy(28,14);
	printf("������������������������");
	gotoxy(28,15);
	printf("������������������������");
	gotoxy(36,16);
	printf("��������");
	gotoxy(36,17);
	printf("��������");
	gotoxy(36,18);
	printf("��������");
	gotoxy(36,19);
	printf("��������");
	
	
	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE),15);
	getch();
	system("cls");
	Menuprincipal();
}


int cuadro()
{
int i,d;

for(i=15;i<65;i++) 
{ 
/*arriba*/ 
gotoxy(i,0); 
printf("%c",205); 
/*abajo*/ 
gotoxy(i,24); 
printf("%c",205); 
}
for(d=0;d<24;d++) 
{ 
/*izquierda*/ 
gotoxy(15,d); 
printf("%c",186); 
/*derecha*/ 
gotoxy(65,d); 
printf("%c",186); 
} 
/*esquinas*/
gotoxy(15,0); 
printf("%c",201); 
gotoxy(65,0); 
printf("%c",187); 
gotoxy(15,24); 
printf("%c",200); 
gotoxy(65,24); 
printf("%c",188); 
}
int validar_num(char numero[30])
{
	int i=0,sn=0,j;
	
	j=strlen(numero);//strlen sirve para contar cuantas letras tienen la cadena
	
	while(i<j && sn==0)
	{
		if(isdigit(numero[i])!=0)//isdigit retorna distinto de 0 si es un numero, si es letra reetorna 0
		{
			i++;
		}
		else
		{
			sn=1;
		}
	}
	return(sn);
}
int validar_cadena(char cadena[30])
{
	int i=0,sw=0,j;
	j=strlen(cadena);//cuenta cuantas letras tiene la cadena
	
	while(i<j && sw==0)
	{
		if(isalpha(cadena[i])!=0)//isalpha si es letra retorna distinto de 0 si es numero retorna 0
			{
				i++;
			}		
		else
		{
			sw=1;
		}
	}
	return(sw);
}
//----------------------------------------------------------------------------------------------------------------------------------

int alta_animal()     
{


	int op=1;   // variable para la opcion
	int anio_act=2015,x=20,y=3;
	int r=1;
	int sw;
	int sn;
	int s=1;
	int c=1;
	int na=1;
	int en=1;
	int nn=1;
	cuadro();
	while(r==1)
	{
		y=3;
		system("cls");
		fflush(stdin);						
        cuadro();
		gotoxy(x,y);
		printf("Ingrese C%cdigo:\n",162);
		y++;
		gotoxy(x,y);
		gets(animales[cp].cod);
		
		sn=validar_num(animales[cp].cod);
		
		if(sn==0)
		{
			gotoxy(x,5);
			printf("Dato v\xA0""lido");
			r++;
		}
		else
		{
			gotoxy(x,5);
			printf("Dato inv\xA0""lido\n");
			gotoxy(x,7);
			printf("Ingrese solo n\xA3meros");
			getch();
		}
		
		
	}
	int pe=1;
	while(pe==1)	
	{
		y=3;
		system("cls");
		fflush(stdin);	
		cuadro();	
		gotoxy(x,y);						
		printf("Ingrese nombre de la mascota:\n ");
		y++;
		gotoxy(x,y);
		gets(animales[cp].nombre);
		
		sw=validar_cadena(animales[cp].nombre);
		if(sw==0)
		{
			gotoxy(x,5);
			printf("Dato v\xA0""lido");
			pe++;
		}
		else
		{
			gotoxy(x,5);
			printf("Dato inv\xA0""lido \n");
			gotoxy(x,7);
			printf("Ingrese solo letras");
			getch();
		}
	}
	
	while (s==1)
	{
	
		y=3;
		system ("cls");
		fflush(stdin);
	    cuadro();
		gotoxy(x,y);
		printf("Ingrese d\xA1""a de nacimiento:\n ");
		y++;
		gotoxy(x,y);
		gets(animales[cp].dia);
		sw=validar_num(animales[cp].dia);
		if (sw==0)
		{
			gotoxy(x,5);
			printf ("Dato v\xA0""lido");
				s++;
		}
		else
		{
		    gotoxy(x,5);
		    printf ("Dato inv\xA0""lido \n");
		    gotoxy(x,7);
		    printf("Ingrese solo numeros");
			getch();
		
		}
	}
	while (c==1)
	
	{
	
		y=3;
		system ("cls");
		fflush(stdin);
		cuadro();
		gotoxy(x,y);
		printf("Ingrese mes de nacimiento:\n ");
		y++;
		gotoxy(x,y);
		gets(animales[cp].mes);
		sw=validar_cadena(animales[cp].mes);
		if (sw==0)
		{
			gotoxy(x,5);
			printf ("Dato v\xA0""lido");
			c++;
		}
		else
		{
			gotoxy(x,5);
			printf ("Dato inv\xA0""lido\n");
			gotoxy(x,7);
			printf("Ingrese solo letras");
			getch();
		}
	}
			y=3;
			system("cls");
			fflush(stdin);
			cuadro();
			gotoxy(x,y);
			printf("Ingrese a\xA4""o de nacimiento: \n ");
			y++;
			gotoxy(x,y);
			scanf ("%d", &animales[cp].anio);
	 
	while (na==1)
	{
		
		y=3;
		system ("cls");
	    fflush(stdin);
	    cuadro();
		gotoxy(x,y);
		printf("Ingrese tipo de animal: \n ");
		y++;
		gotoxy (x,y);
		gets(animales[cp].tipo);
		sw=validar_cadena(animales[cp].tipo);
		if (sw==0)
		{
			gotoxy(x,5);
			printf ("Dato v\xA0""lido");
			na++;
		}
		else
		{
			    gotoxy(x,5);
				printf ("Dato inv\xA0""lido\n");
				printf("Ingrese solo letras");
				gotoxy(x,7);
				getch();
		}
	}
		int em=1;
	while (em==1)
	{
		y=3;
		system ("cls");
		fflush(stdin);
		cuadro();
		gotoxy(x,y);
		printf("Ingrese nombre del due\xA4""o:");
		y++;
		gotoxy (x,y);
		gets(animales[cp].duenio);
		sw=validar_cadena(animales[cp].duenio);
		if (sw==0)
		{
			gotoxy(x,5);
			printf ("Dato v\xA0""lido");
			em++;
		}
		else
		{
			gotoxy(x,5);
			printf ("Dato inv\xA0""lido\n");
			gotoxy(x,7);
			printf("Ingrese solo letras");
			getch();
			
		}
	}
			
	 while (en==1)
	{
			
	
		system ("cls");
		fflush(stdin);
		cuadro();
		gotoxy(20,3);
	    
		printf("Ingrese raza del animal: \n ");
		y++;
		gotoxy (20,4);
		gets(animales[cp].raza);
		sw=validar_cadena(animales[cp].raza);
		if (sw==0)
		{
			gotoxy(x,5);
			printf ("Dato v\xA0""lido");	
			en++;	
		}
		else
		{
			gotoxy(x,5);
			printf ("Dato inv\xA0""lido\n");
			gotoxy(x,7);
			printf("Ingrese solo letras");
			getch();
		}
	
	}
	while (nn==1)
	{
		y=3;
		system ("cls");
		fflush(stdin);
		cuadro();
		gotoxy(20,3);
		printf("Ingrese servicio que recibi\xA2""o el animal");
		y++;
		gotoxy(20,4);
		gets(animales[cp].servicio);
		sw=validar_cadena(animales[cp].servicio);
		if (sw==0)
		{
			gotoxy(x,5);
			printf ("Dato v\xA0""lido");
			nn++;
		}
		else
		{
			gotoxy(x,5);
			printf ("Dato Inv\xA0""lido\n");
			gotoxy(x,7);
			printf("Ingrese solo letras");
			getch();
		}
	}
	    system("cls");
	    cuadro();
		animales[cp].edad=anio_act-animales[cp].anio;    // calcula la edad de animales
		animales[cp].bajlog=0;                           // asigna 0 a la baja logica
		cp++;
		y++;
		gotoxy(x,y);
		printf("\250Desea ingresar otro dato? 1-si o 2-no");
		printf("\n");
		y++;
		gotoxy(x,y);
		scanf("%d",&op);
		
		switch(op)
			{
				case 1:
					system("cls");
					alta_animal();
					break;
 				case 2:
					system("cls");
					Admin();
					break;
				default:
					printf("Error en el ingreso\n");	
					getch();
					scanf("%d",&op);		//Asi no se tilda el switch si mandan algo erroneo			
			}	
		Admin();
		getch();

}  // Cierre del proceso alta_animal
//----------------------------------------------------------------------------------------------------------------------------------
int alta_duenio ()
{
	int op,x=20,y=3;
	int r=1;
	int sn, sw;
	cuadro();
	while(r==1)
	{
		y=3;
		system("cls");
		fflush(stdin);						
        cuadro();
		gotoxy(x,y);
		printf("Ingrese C\xA2""digo:\n");
		y++;
		gotoxy(x,y);
		gets(persona[cd].codigo);
		sn=validar_num(persona[cd].codigo);
		
		if(sn==0)
		{
			gotoxy(x,5);
			printf("Dato v\xA0""lido");
			r++;
		}
		else
		{
			gotoxy(x,5);
			printf("Dato inv\xA0""lido\n");
			gotoxy(x,7);
			printf ("Ingrese solo n\xA3meros");
			getch();
		}
		
		
	}
	    
	    int rs=1;
	    while(rs==1)
	    {
	    	y=3;
			system("cls");
			fflush(stdin);						
            cuadro();
			gotoxy(x,y);
			printf ("Ingrese nombre del due\xA4""o: \n");
			y++;
			gotoxy(x,y);
			gets(persona[cd].nombre);
			sw=validar_cadena(persona[cd].nombre);
			if (sw==0)
			{
				gotoxy(x,5);
				printf ("Dato v\xA0""lido");
				rs++;
			}		
			else
			{
				gotoxy(x,5);
				printf ("Dato inv\xA0""lido\n");
				gotoxy(x,7);
				printf("Ingrese solo letras");
				getch();
			}
		
		}
		
	    int st=1;
	while (st==1)
	{
			y=3;
			system("cls");
			cuadro();
			fflush(stdin);						
			gotoxy(x,y);
			printf ("Ingrese apellido del due\xA4""o: \n");
			y++;
			gotoxy(x,y);
			gets(persona[cd].apellido);
			sw=validar_cadena(persona[cd].apellido);
			if (sw==0)
			{
				gotoxy(x,5);
				printf ("Dato v\xA0""lido");
				st++;
			}	
			else
			{
				gotoxy(x,5);
				printf ("Dato inv\xA0""lido\n");
				gotoxy(x,7);
				printf("Ingrese solo letras");
				getch();
			}
	}
	
		int sd=1;
		while (sd==1)
		{
			
			y=3;
			system("cls");
			fflush(stdin);						
			cuadro();
			gotoxy(x,y);
			printf ("Ingrese tipo de documento: \n");
			y++;
			gotoxy(x,y);
			gets(persona[cd].tipo);
			sw=validar_cadena(persona[cd].tipo);
			if (sw==0)
			{
				gotoxy(x,5);
				printf ("Dato v\xA0""lido");
				sd++;
			}
			else 
			{
				gotoxy(x,5);
				printf ("Dato inv\xA0""lido\n");
				gotoxy(x,7);
				printf("Ingrese solo letras");
				getch();
				
			}
		}
		
		int da=1;
		while (da==1)
		{
			y=3;
			system("cls");
			fflush(stdin);						
			cuadro();
			gotoxy(x,y);
			printf ("Ingrese n\xA3mero de documento: \n");
			y++;
			gotoxy(x,y);
			gets (persona[cd].documento);
			sn=validar_num(persona[cd].documento);
			if (sn==0)
			{
				gotoxy(x,5);
				printf ("Dato v\xA0""lido");
				da++;
			}
			else
			{
				gotoxy(x,5);
				printf ("Dato inv\xA0""lido\n");
				gotoxy(x,7);
				printf ("Ingrese solo n\xA3meros");
				getch ();
			}
		}
		
		int cal=1;
		while (cal==1)
		{	
			y=3;
			system("cls");
			fflush(stdin);						
			cuadro();
			gotoxy(x,y);
			printf ("Ingrese Tel\x82""fono: \n");
			y++;
			gotoxy(x,y);
			gets(persona[cd].telefono);
			sn=validar_num(persona[cd].telefono);
			if (sn==0)
			{
				gotoxy(x,5);
				printf ("Dato v\xA0""lido");
				cal++;
			}
			else
			{
				gotoxy(x,5);
				printf ("Dato inv\xA0""lido\n");
				gotoxy(x,7);
				printf ("Ingrese solo n\xA3meros");
				getch();
			}
		}
		
		
	    y=3;
		system("cls");
		fflush(stdin);						
		cuadro();
		gotoxy(x,y);
		printf ("Ingrese Direcci\xA2""n: \n",162);
		y++;
		gotoxy(x,y);
		gets(persona[cd].direccion);
			
		system("cls");
		cuadro();
		y++;
		gotoxy(x,y);
		persona[cd].bajlog=0;
		cd++;
		gotoxy(x,7);
		printf("\250Desea ingresar otro dato? 1-Si o 2-No \n");
		printf("\n");
		gotoxy(x,8);
		scanf("%d",&op);		
		switch(op)
			{
				case 1:
					system("cls");
					alta_duenio();
					break;
				case 2:
					system("cls");
					Admin();
					break;
				    
				default:
					gotoxy(x,9);
					printf("Error en el ingreso\n");	
					getch();
					scanf("%d",&op);					
			}	
		Admin();			// vuelve al menu
		getch();
}  					// cierre del alta_due�o
//----------------------------------------------------------------------------------------------------------------------------------
int lista_animal()
{
	int k,op,x=0,y=1;
	
	gotoxy(0,0);
	printf("��������������������������������������������������������������������������������");
	gotoxy(1,y);
	printf ("Edad \n");
	gotoxy(9,y);
	printf("Nombre\n");
	gotoxy(18,y);
	printf("D\xA1""a");
	gotoxy(22,y);
	printf("Mes\n");
	gotoxy(31,y);
	printf("A\xA4""o\n");
	gotoxy(39,y);
	printf("Tipo de mascota\n");
	gotoxy(57,y);
	printf("Raza\n",162);
	gotoxy(68,y);
	printf("C\xA2""digo\n",162);
	gotoxy(0,y);
	printf("�");//linea 1 
	gotoxy(8,y);
	printf("�");//linea 2 
	gotoxy(17,y);
	printf("�");//linea 3 
	gotoxy(21,y);
	printf("�");//linea 4 
	gotoxy(30,y);
	printf("�");//linea 5 
	gotoxy(38,y);
	printf("�");//linea 6 
	gotoxy(56,y);
	printf("�");//linea 7 
	gotoxy(67,y);
	printf("�");//linea 8 
	gotoxy(79,y);
	printf("�");//linea final
	y++;
	gotoxy(0,y);
	printf("��������������������������������������������������������������������������������");
	
	for(k=0;k<cp;k++)
	{
		if(animales[k].bajlog==0)
		{
		
		y++;
		
		gotoxy(1,y); 
		printf("%d",animales[k].edad);
		gotoxy(9,y);
		printf("%s",animales[k].nombre);
		gotoxy(18,y);
		printf("%s",animales[k].dia);
		gotoxy(22,y);
		printf("%s",animales[k].mes);
		gotoxy(31,y);
		printf("%d",animales[k].anio);
		gotoxy(39,y);
		printf("%s",animales[k].tipo);
		gotoxy(57,y);
		printf("%s",animales[k].raza);
		gotoxy(68,y);	
		printf("%s",animales[k].cod);	
	

	gotoxy(0,y);
	printf("�");//linea 1 
	gotoxy(8,y);
	printf("�");//linea 2 
	gotoxy(17,y);
	printf("�");//linea 3 
	gotoxy(21,y);
	printf("�");//linea 4        
	gotoxy(30,y);
	printf("�");//linea 5 
	gotoxy(38,y);
	printf("�");//linea 6 
	gotoxy(56,y);
	printf("�");//linea 7 
	gotoxy(67,y);
	printf("�");//linea 8 
	gotoxy(79,y);
	printf("�");//linea final

	 }
	}
	printf("��������������������������������������������������������������������������������");
	getch();
	

	gotoxy(0,y+1);
	printf("\nPara regresar al men\xA3"" ingrese 1 \n");
	scanf("%d",&op);
	switch(op)
	{
		case  1:
		system("cls");
		if(mp==0)
			{
			Admin();
			break;
			}
			else
			{
			Usuario();
			break;
			}	
	}
	getch();
}							//cierre del proceso listado
//----------------------------------------------------------------------------------------------------------------------------------
int menu_alta()     // menu alta
{
	int x=20,y=5;

	int o;
	cuadro();
	gotoxy(30,2);
	printf ("Men\xA3"" Altas\n");
	gotoxy(30,3);
      printf("---- -----\n\n\n");
	gotoxy(x,y);
	printf("1-Alta due\xA4""o\n");
	y++;
	gotoxy(x,y);
	printf("2-Alta animales\n");
	y++;
	gotoxy(x,y);
	printf("3-Volver\n");
	y++;
	gotoxy(x,y);
	scanf("%d",&o);
	switch(o)
	{
		case 1:
			system("cls");
			alta_duenio();
			break;
		case 2:
			system("cls");
			alta_animal();
			break;
		case 3:
			system("cls");
			Admin();
			break;
		default:
			printf("Error de ingreso");
			getch();
			menu_alta();
	}
	
}

int lis_du()

{
	int j,qq,resul,op,x=0,y=1,i;
	for (j=0;j<cd-1;j++)
	{	
		for (qq=j+cd;qq<cd;qq++)
		{
		 	resul=strcmp(persona[j].apellido,persona[qq].apellido);
		
			if(resul==1)

			{
				strcpy(aux.apellido,persona[j].apellido);
  			  	strcpy(persona[j].apellido,persona[qq].apellido);
 			   	strcpy(persona[qq].apellido,aux.apellido);
     	
		     	strcpy(aux.nombre,persona[j].nombre);
    			strcpy(persona[j].nombre,persona[qq].nombre);
    			strcpy(persona[qq].nombre,aux.nombre);	
     
    			strcpy(aux.codigo,persona[j].codigo);
    			strcpy(persona[j].codigo,persona[qq].codigo);
    			strcpy(persona[qq].codigo,aux.codigo);    	
    	
    			strcpy(aux.tipo,persona[j].tipo);
    			strcpy(persona[j].tipo,persona[qq].tipo);
    			strcpy(persona[qq].tipo,aux.tipo);   	
    	
    			strcpy(aux.documento,persona[j].documento);
    			strcpy(persona[j].documento,persona[qq].documento);
    			strcpy(persona[qq].documento,aux.documento); 
    			
    			strcpy(aux.telefono,persona[j].telefono);
    			strcpy(persona[j].telefono,persona[qq].telefono);
    			strcpy(persona[qq].telefono,aux.telefono);
    			
    			strcpy(aux.direccion,persona[j].direccion);
    			strcpy(persona[j].direccion,persona[qq].direccion);
    			strcpy(persona[qq].direccion,aux.direccion);
	        }
  	    }
 	}

	
	gotoxy(0,0);
	printf("��������������������������������������������������������������������������������");
	gotoxy(1,y);
	printf("Apellido\n");
	gotoxy(15,y);
	printf("Nombre\n");
	gotoxy(27,y);
	printf("C\xA2""digo\n");
	gotoxy(35,y);
	printf("Tipo doc.\n");
	gotoxy(46,y);
	printf("N\xA3mero Doc.\n");
	gotoxy(59,y);
	printf("T\x82""lefono\n");
	gotoxy(70,y);
	printf("Direc.\n");
	
	gotoxy(0,y);
	printf("�");//linea 1 izquierda
	gotoxy(14,y);
	printf("�");//linea 2 izquierda
	gotoxy(26,y);
	printf("�");//linea 3 izquierda
	gotoxy(34,y);
	printf("�");//linea 4 izquierda
	gotoxy(45,y);
	printf("�");//linea 5 izquierda
	gotoxy(58,y);
	printf("�");//linea 6 izquierda
	gotoxy(69,y);
	printf("�");//linea 7 izquierda
	gotoxy(79,y);
	printf("�");//linea final
	y++;
	gotoxy(0,y);
	printf("��������������������������������������������������������������������������������");
	
	for(i=0;i<cd;i++)
	{
		if(persona[i].bajlog==0)
	 {	
		y++;
		gotoxy(1,y);
		printf ("%s\n",persona[i].apellido);
		gotoxy(15,y);
		printf("%s\n",persona[i].nombre);
		gotoxy(27,y);
		printf ("%s\n",persona[i].codigo);
		gotoxy(35,y);
		printf ("%s\n",persona[i].tipo);
		gotoxy(46,y);
		printf("%s\n",persona[i].documento);
		gotoxy(59,y);
		printf("%s\n",persona[i].telefono);
		gotoxy(70,y);
		printf("%s\n",persona[i].direccion);
		
		gotoxy(0,y);
		printf("�");//linea 1 izquierda
		gotoxy(14,y);
		printf("�");//linea 2 izquierda
		gotoxy(26,y);
		printf("�");//linea 3 izquierda
		gotoxy(34,y);
		printf("�");//linea 4 izquierda            
		gotoxy(45,y);
		printf("�");//linea 5 izquierda
		gotoxy(58,y);
		printf("�");//linea 6 izquierda
		gotoxy(69,y);
		printf("�");//linea 7 izquierda
		gotoxy(79,y);
	    printf("�");//linea final
		
	   
		
	 }  // cierre del if
	}  // cierre del for
	printf("��������������������������������������������������������������������������������");
	getch();
	

gotoxy(0,y+1);
	printf("\nPara regresar al men\xA3"" ingrese 1 \n");
	scanf("%d",&op);
	switch(op)
	{
		case  1:
		if(mp==0)
		{
		Admin();
		break;
        }
        else
        {
        Usuario();
        break;
        }
		
		default:
			printf("Error de ingreso");
			if(mp==0)
			{
			getch();
			Admin();
			}
			else
			{
			getch();
			Usuario();
			}
			
	}
	getch();
	
}
	

//----------------------------------------------------------------------------------------------------------------------------------
int menu_busqueda()   // menu de las busquedas
{
	int x=20, y=5;
	system("cls");
	cuadro();
	gotoxy(30,2);
    printf ("Men\xA3"" Busqueda\n");
	gotoxy(30,3);
    printf("---- --------\n\n\n");
	gotoxy(x,y);
	printf("1- Buscar por c\xA2""digo due\xA4""o \n");
	y++;
	gotoxy(x,y);
	printf("2- Buscar por apellido due\xA4""o \n");
	y++;
	gotoxy(x,y);
	printf("3- Buscar por n\xA3mero de documento due\xA4""o \n");
	y++;
	gotoxy(x,y);
	printf("4- Buscar por nombre del animal \n");
	y++;
	gotoxy(x,y);
	printf("5- Buscar por c\xA2""digo del animal \n");
	y++;
	gotoxy(x,y);
	printf("6- Volver \n");
	y++;
	gotoxy(x,y);
	printf("Ingrese opci\xA2""n deseada \n");
	y++;
	gotoxy(x,y);
	scanf("%d",&o);
	switch (o)
	{
		case 1:
			system("cls");
			bus_duenio_cod();
			break;
		
		case 2:
			system("cls");
			bus_duenio_ap();
			break;
			
		case 3:
			system("cls");
			bus_duenio_doc();
			break;
		
		case 4:
			system("cls");
			bus_animal_nom();
			break;
		
		case 5:
			system("cls");
			bus_animal_cod();
			break;
		
		case 6:
			system("cls");
			if(mp==0)
			{
			Admin();
			break;
			}
			else
			{
				Usuario();
				break;
			}
		default:
		    printf("Error de Ingreso");
		    getch();
		    system("cls");
		    menu_busqueda();
		    break;
			
	}  // cierre del switch
}
//----------------------------------------------------------------------------------------------------------------------------------
int menu_modificaciones()

{
	int x=20, y=5;
	
	system("cls");
	cuadro();
	gotoxy(30,2);
    printf ("Men\xA3"" Modificaciones\n");
	gotoxy(30,3);
    printf("---- --------------\n\n\n");
	gotoxy(x,y);
	printf("1-Modificar datos due\xA4""o \n");
	y++;
	gotoxy(x,y);
	printf("2-Modificar datos animal\n");
	y++;
	gotoxy(x,y);
	printf("3-Volver\n");
	y++;
	gotoxy(x,y);
	printf("Ingrese opci\xA2""n deseada \n",162);
	y++;
	gotoxy(x,y);
	scanf("%d",&o);
	switch (o)
	{
		case 1:
			system("cls");
			modificar_duenio();
			break;
		
		case 2:
			system("cls");
			modificar_animal();
			break;
		
		case 3:
			system("cls");
			Admin();
			break;
		
		default:
			gotoxy(x,13);
			printf("Error de Ingreso");
		    getch();
		    system("cls");
		    menu_modificaciones();
		    break;
		    
		    
	}
	
}
//----------------------------------------------------------------------------------------------------------------------------------
int bus_duenio_cod()
{
	
	int i, op, cont=0, x=20, y=5;
	char cod[20];
	do
	{
	system("cls");
	cuadro();
	gotoxy(25,2);
	printf("Ingrese c\xA2""digo del due\xA4""o a buscar:");
	gotoxy(25,3);
	scanf("%s",cod);
	
	for(i=0; i<cd; i++)
	 {
	  if(!strcmp(persona[i].codigo,cod))
	  {
	  	system("cls");
	  	cuadro();
		gotoxy(30,2);
	  	printf("---Datos Due\xA4""o--- \n");
	  	gotoxy(x,y);
	  	printf("C\xA2""digo:%s\n", persona[i].codigo);
	  	y++;
	  	gotoxy(x,y);
	  	printf("Nombre:%s\n", persona[i].nombre);
	  	y++;
	  	gotoxy(x,y);
	  	printf("Apellido:%s\n", persona[i].apellido);
	  	y++;
	  	gotoxy(x,y);
		printf("Tipo de documento:%s\n", persona[i].tipo);
		y++;
		gotoxy(x,y);
		printf("N\xA3mero de Documento:%s\n", persona[i].documento);
		y++;
		gotoxy(x,y);
		printf("Tel\x82""fono:%s\n", persona[i].telefono);
		y++;
		gotoxy(x,y);
		printf("Direcci\xA2""n:%s\n", persona[i].direccion);
		y++;
		cont++;	  	
	  }
	
	 }
    if(cont==0)
    {
    	gotoxy(x,y);
    	printf("No se encontr\xA2"" el c\xA2""digo\n\n");
    	y++;
	}
	gotoxy(x,y);
	printf("\250Desea buscar otro c\xA2""digo? 1-Si 2-No\n");
	y++;
	gotoxy(x,y);
	scanf("%d",&op);
	} while(op==1);
	
}
//----------------------------------------------------------------------------------------------------------------------------------
int bus_duenio_doc()
{
	int i, op, cont=0, x=20, y=5;
	char doc[20];
	do
	{
	system("cls");
	cuadro();
	gotoxy(25,2);
	printf("Ingrese n\xA3mero de documento del due\xA4""o a buscar: \n");
	gotoxy(25,3);
	scanf("%s",doc);

	for(i=0; i<cd; i++)
	 {
	  if(!strcmp(persona[i].documento,doc))
	  {
	  	system("cls");
	  	cuadro();
	  	gotoxy(30,2);
	  	printf("Datos due\xA4""o \n");
	  	gotoxy(x,y);
	  	printf("C\xA2""digo: %s\n", persona[i].codigo);
	  	y++;
	  	gotoxy(x,y);
	  	printf("Nombre:%s\n", persona[i].nombre);
	  	y++;
	  	gotoxy(x,y);
	  	printf("Apellido:%s\n", persona[i].apellido);
	  	y++;
	  	gotoxy(x,y);
		printf("Tipo de documento:%s\n", persona[i].tipo);
		y++;
		gotoxy(x,y);
		printf("N\243mero de documento:%s\n", persona[i].documento);
		y++;
		gotoxy(x,y);
		printf("Tel\x82""fono:%s\n", persona[i].telefono);
		y++;
		gotoxy(x,y);
		printf("Direcci\xA2""n:%s\n", persona[i].direccion);
		y++;
		
		cont++;	  	
	  }
	
	 }
    if(cont==0)
    {
    	gotoxy(x,y);
    	printf("No se encontr\xA2"" el documento\n\n");
    	y++;
	}
	gotoxy(x,y);
	printf("\250Desea buscar otro documento? 1-Si 2-No\n");
	y++;
	gotoxy(x,y);
	scanf("%d",&op);
	} while(op==1);
}
//----------------------------------------------------------------------------------------------------------------------------------
int bus_duenio_ap()
{
	
	int i, op, cont=0, x=20 ,y=5;
	char ap[100];
	do
	{
	system("cls");
	cuadro();
	gotoxy(25,2);
	printf("Ingrese apellido del due\xA4""o a buscar: \n ");
	gotoxy(25,3);
	scanf("%s",ap);
	
	for(i=0; i<cd; i++)
	 {
	  if(strcmp(persona[i].apellido,ap)==0)
	  {
	  	cuadro();
	  	gotoxy(30,2);
	  	printf("Datos due\xA4""o \n");
	  	gotoxy(x,y);
	  	printf("C\xA2""digo:%s\n", persona[i].codigo);
	  	y++;
	  	gotoxy(x,y);
	  	printf("Nombre:%s\n", persona[i].nombre);
	  	y++;
	  	gotoxy(x,y);
	  	printf("Apellido:%s\n", persona[i].apellido);
	  	y++;
	  	gotoxy(x,y);
		printf("Tipo de documento:%s\n", persona[i].tipo);
		y++;
		gotoxy(x,y);
		printf("N\xA3mero de Documento:%s\n", persona[i].documento);
		y++;
		gotoxy(x,y);
		printf("Tel\x82""fono:%s\n", persona[i].telefono);
		y++;
		gotoxy(x,y);
		printf("Direcci\xA2""n:%s\n", persona[i].direccion);
		y++;
		
		cont++;	  	
	  }
	
	 }
    if(cont==0)
    {
    	gotoxy(x,y);
    	printf("No se encontr\xA2"" el apellido\n\n");
    	y++;
	}
	gotoxy(x,y);
	printf("\250Desea buscar otro apellido? 1-Si 2-No\n");
	y++;
	gotoxy(x,y);
	scanf("%d",&op);
	} while(op==1);
}

//----------------------------------------------------------------------------------------------------------------------------------
int bus_animal_cod()
{
	char cod_animal[23]; 
	int op, cont=0, g, x=20, y=5;

	do
	{
	system("cls");
	cuadro();
	gotoxy(25,2);
	printf("Ingrese c\xA2""digo del animal a buscar");
	gotoxy(25,3);
	scanf("%s",cod_animal);
	
	for(g=0; g<cp; g++)
	{
	  if(!strcmp(animales[g].cod,cod_animal))
	  {
	  	system("cls");
	  	cuadro();
	  	gotoxy(30,2);
	  	printf("---Datos Animal---\n");
	  	y++;
	  	gotoxy(x,y);
	  	printf("C\xA2""digo: %s\n", animales[g].cod);
	  	y++;
	  	gotoxy(x,y);
	  	printf("Nombre: %s\n", animales[g].nombre);
	  	y++;
	  	gotoxy(x,y);
	  	printf("D\xA1""a de Nacimiento: %s\n", animales[g].dia);
	  	y++;
	  	gotoxy(x,y);
		printf("Mes de Nacimiento: %d\n", animales[g].mes);
		y++;
		gotoxy(x,y);
		printf("A\xA4""o de Nacimiento: %d\n", animales[g].anio);
		y++;
		gotoxy(x,y);
		printf("Due\xA4""o: %s\n", animales[g].duenio);
		y++;
		gotoxy(x,y);
		printf("Tipo de mascota: %s\n", animales[g].tipo);
		y++;
		gotoxy(x,y);
		printf("Raza: %s\n", animales[g].raza);
		y++;
		gotoxy(x,y);
		printf("Edad: %d\n", animales[g].edad);
		y++;
		gotoxy(x,y);
		printf("Servicio: %s\n", animales[g].servicio);
		y++;
		getch();
		cont++;	  	
	  }
	}
	if(cont==0)
    {
    	gotoxy(x,y);
    	printf("No se encontr\xA2"" el c\xA2""digo\n\n");
    	y++;
	}
	gotoxy(x,y);
	printf("\250Desea buscar otro c\xA2""digo? 1-Si 2-No\n");
	y++;
	gotoxy(x,y);
	scanf("%d",&op);
	} while(op==1);

}
//----------------------------------------------------------------------------------------------------------------------------------
int bus_animal_nom()
{ 
	int op, cont=0, g, x=20, y=5;
    char nom[100];
	do
	{
	system("cls");
	cuadro();
	gotoxy(25,2);
	printf("Ingrese nombre del animal a buscar");
	gotoxy(25,3);
	scanf("%s",nom);
	
	for(g=0; g<cp; g++)
	{
	  if(strcmp(animales[g].nombre,nom)==0)
	  {
	  	system("cls");
	  	cuadro();
	  	gotoxy(30,2);
	  	printf("---Datos Animal--- \n");
	  	y++;
	  	gotoxy(x,y);
	  	printf("C\xA2""digo: %s\n", animales[g].cod);
	  	y++;
	  	gotoxy(x,y);
	  	printf("Nombre: %s\n", animales[g].nombre);
	  	y++;
	  	gotoxy(x,y);
	  	printf("D\xA1""a de Nacimiento: %s\n", animales[g].dia);
	  	y++;
	  	gotoxy(x,y);
		printf("Mes de Nacimiento: %s\n", animales[g].mes);
		y++;
		gotoxy(x,y);
		printf("A\xA4""o de Nacimiento: %d\n", animales[g].anio);
		y++;
		gotoxy(x,y);
		printf("Due\xA4""o: %s\n", animales[g].duenio);
		y++;
		gotoxy(x,y);
		printf("Tipo de mascota: %s\n", animales[g].tipo);
		y++;
		gotoxy(x,y);
		printf("Raza: %s\n", animales[g].raza);
		y++;
		gotoxy(x,y);
		printf("Edad: %d\n", animales[g].edad);
		y++;
		gotoxy(x,y);
		printf("Servicio: %s\n", animales[g].servicio);
		
		getch();
		cont++;	  	
	  }
	}
	if(cont==0)
    {
    	gotoxy(x,y);
    	printf("No se encontr\xA2"" el nombre\n\n");
    	y++;
	}
	gotoxy(x,y);
	printf("\250Desea buscar otro nombre? 1-Si 2-No\n");
	y++;
	gotoxy(x,y);
	scanf("%d",&op);
	} while(op==1);

}
//----------------------------------------------------------------------------------------------------------------------------------
int modificar_duenio()
{
	
	int s;
	int i;
	char cod_bus[20];
	int cont1=0;
	int op;
	int error;
	int x=20;
	int y=5;
	cuadro();
	do
	{
		system("cls");
		cuadro();
		gotoxy(20,2);
		printf("Ingresar c\xA2""digo del due\xA4""o a modificar:");
		gotoxy(20,3);
		scanf("%s",cod_bus);
		
		for(i=0;i<cd;i++)
	    {
		   if(!strcmp(persona[i].codigo,cod_bus))
           {
           	system("cls");
           	cuadro();
           	gotoxy(30,2);
           	printf("---Datos actuales del due\xA4""o---\n"); 
				gotoxy(x,y);
				printf("1-C\xA2""digo: %s \n",persona[i].codigo); 
				y++;
				gotoxy(x,y);
				printf("2-Apellido: %s \n",persona[i].apellido); 
				y++;
				gotoxy(x,y);
				printf("3-Nombre: %s \n",persona[i].nombre);
				y++;
				gotoxy(x,y);
				printf("4-Tipo de Documento: %s \n",persona[i].tipo);
				y++;
				gotoxy(x,y);
				printf("5-N\xA3mero de Documento: %s \n",persona[i].documento);
				y++;
				gotoxy(x,y);
				printf("6-Tel\x82""fono: %s \n",persona[i].telefono);
				y++;
				gotoxy(x,y);
				printf("7-Direcci\xA2""n: %s \n",persona[i].direccion);
				y++;
				gotoxy(x,y);
				printf("8-Salir: \n");
				y++;
				gotoxy(x,y);
				printf("Ingrese opci\xA2""n a modificar: \n");
				y++;
				gotoxy(x,y);
				scanf("%d",&s);
				system("cls");
				cont1++;
		    
		
		cuadro();
    			
    	switch (s)   			
	{
		case 1: 
		gotoxy(20,2);				
		printf ("Ingrese nuevo c\xA2""digo\n");
		gotoxy(20,3);	
    	scanf("%d",&persona[i].codigo);	
    	gotoxy(20,4);
		printf("\nDatos actualizados!"); 
		break;
								
		case 2:
		gotoxy(20,2);				
		printf("Ingrese nuevo apellido: \n");
		gotoxy(20,3);	
    	scanf("%s",persona[i].apellido);
    	gotoxy(20,4);
    	printf("Datos actualizados!");
    	break;
    						
    	case 3:												
		gotoxy(20,2);					
		printf("Ingrese nuevo nombre: \n");
		gotoxy(20,2);	
    	scanf("%s",persona[i].nombre);
    	gotoxy(20,4);
		printf("Datos actualizados!");
		break;						
    							
    	case 4: 
    	gotoxy(20,2);			
    	printf ("Ingrese nuevo tipo de documento\n");
    	gotoxy(20,3);	
    	scanf ("%s",persona[i].tipo);
    	gotoxy(20,4);	
    	printf("Datos actualizados!");
    	break;
    																		
    	case 5:
		gotoxy(20,2);					
		printf ("Ingrese nuevo N\xA3mero de documento: \n");
		gotoxy(20,3);	
    	scanf ("%s",persona[i].documento);
    	gotoxy(20,4);
    	printf("Datos actualizados!");
    	break;
    	
    	case 6:
		gotoxy(20,2);						
    	printf ("Ingrese nuevo tel\x82""fono: \n");
    	gotoxy(20,2);	
    	scanf ("%s",persona[i].telefono);
    	gotoxy(20,4);
    	printf("Datos actualizados!");
    	break;
    							
    	case 7:
    	gotoxy(20,2);						
    	printf ("Ingrese nueva direcci\xA2""n: \n");
    	gotoxy(20,3);	
    	scanf ("%s",persona[i].direccion);
    	gotoxy(20,4);
    	printf("Datos actualizados!");
    	break;
    	
    	case 8:
    	system("cls");
    	Admin();
    	break;
    	
    	default:
    	gotoxy(20,2);	
    	printf("Error de ingreso");
    	getch();
    	modificar_duenio();
		break;
	}
  }
}

    if(cont1==0)
	    {
		    gotoxy(20,8);    	
	    	printf("El due\xA4""o que desea modificar no existe.\n");
	    	gotoxy(20,9);
	    	printf("\250Intentar de nuevo? 1-Si 2-No \n");
	    	gotoxy(20,10);
    	    scanf("%d",&error);
   	        if(error==1)
   	        {
		       modificar_duenio();
	        }
	        else
	        {
    	       Admin();
		    }
	    	
		}

		else
		{
			gotoxy(20,8);
           printf("\250Desea modificar otro due\xA4""o? 1-Si 2-No \n");
           gotoxy(20,9);
	       scanf("%d",&op);
        }
	}while(op==1);
	if(op==2)
	{
		Admin();
	}
	else
	{
		Admin();
	}
			
}
//----------------------------------------------------------------------------------------------------------------------------------
int modificar_animal()
{
	
	int s;
	int i;
	char cod_ani[22];
	int cont2=0;
	int op=0;
	int error;
	int x=20;
	int y=5;
	cuadro();
	do
	{
	
		system("cls");
		cuadro();
		gotoxy(20,2);
		printf("Ingresar c\xA2""digo del animal a modificar:");
		gotoxy(20,3);
		scanf("%s",cod_ani);
		
		for(i=0;i<cp;i++)
	    {
		   if(!strcmp(animales[i].cod,cod_ani))
           {
           		system("cls");
           		cuadro();
				gotoxy(30,2);
           		printf("---Datos actuales del Animal---\n"); 
				gotoxy(x,y);
				printf("0-c\xA2""digo: %s \n",animales[i].cod);
				y++; 
				gotoxy(x,y);
				printf("1-Nombre: %s \n",animales[i].nombre);
				y++; 
				gotoxy(x,y);	
				printf("2-Due\xA4""o: %s \n",animales[i].duenio);
				y++;
				gotoxy(x,y);	
				printf("3-D\xA1""a de Nacimiento: %s \n",animales[i].dia);
				y++;
				gotoxy(x,y);	
				printf("4-Mes de Nacimiento: %s \n",animales[i].mes);
				y++;
				gotoxy(x,y);	
				printf("5-A\xA4""o  de Nacimiento: %d \n",animales[i].anio);
				y++;
				gotoxy(x,y);	
				printf("6-Tipo de Animal: %s \n",animales[i].tipo);
				y++;
				gotoxy(x,y);	
				printf("7-Raza del Animal: %s \n",animales[i].raza);
				y++;
				gotoxy(x,y);	
				printf("8-Edad del Animal: %d \n",animales[i].edad);
				y++;
				gotoxy(x,y);
				printf("9-Servicio: %s \n",animales[i].servicio);
				y++;
				gotoxy(x,y);
				printf("-Para salir precione una letra aleatoria");
				y++;
				gotoxy(x,y);
				printf("Ingrese opci\xA2""n a modificar: \n");
				y++;
				gotoxy(x,y);
				scanf("%d",&s);
				system("cls");
				cuadro();
				cont2++;
		    
				
    			
    	switch (s)   			
	{
		case 0: 
		gotoxy(20,2);		
		printf ("Ingrese nuevo c\xA2""digo\n");
		gotoxy(20,3);
    	scanf ("%d",&animales[i].cod);	
    	gotoxy(20,4);
		printf("Datos actualizados!\n");
		break;
								
		case 1:
		gotoxy(20,2);			
		printf("Ingrese nuevo nombre: \n");
		gotoxy(20,3);	
    	scanf("%s",animales[i].nombre);
    	gotoxy(20,4);
    	printf("Datos actualizados!\n");
    	break;
    						
    	case 2:												
		gotoxy(20,2);					
		printf("Ingrese nuevo due\xA4""o: \n");
		gotoxy(20,3);	
    	scanf("%s",animales[i].duenio);
    	gotoxy(20,4);
		printf("Datos actualizados!\n");
		break;						
    							
    	case 3: 
		gotoxy(20,2);			
    	printf ("Ingrese nuevo d\xA1""a de nacimiento\n");
    	gotoxy(20,3);	
    	scanf ("%d",&animales[i].dia);
    	gotoxy(20,4);
    	printf("Datos actualizados!\n");
    	break;
    																		
    	case 4:
		gotoxy(20,2);				
		printf ("Ingrese nuevo mes de nacimiento: \n");
		gotoxy(20,3);	
    	scanf ("%s",animales[i].mes);
    	gotoxy(20,4);
    	printf("Datos actualizados!\n");
    	break;
    	
    	case 5:
		gotoxy(20,2);					
    	printf ("Ingrese nuevo a\xA4""o de nacimiento: \n");
    	gotoxy(20,3);	
    	scanf ("%d",&animales[i].anio);
    	gotoxy(20,4);
    	printf("Datos actualizados!\n");
    	break;
    							
    	case 6:
		gotoxy(20,2);					
    	printf ("Ingrese nuevo tipo de animal: \n");
    	gotoxy(20,3);	
    	scanf ("%s",animales[i].tipo);
    	gotoxy(20,4);
    	printf("Datos actualizados!\n");
    	break;
    	
    	case 7:
    	gotoxy(20,2);	
    	printf("Ingrese nueva raza: \n");
        gotoxy(20,3);	
    	scanf("%s",animales[i].raza);
    	gotoxy(20,4);
    	printf("Datos actualizados!\n");
    	break;
    	
    	case 8:
    	gotoxy(20,2);	
    	printf("Ingrese nueva edad \n");
    	gotoxy(20,3);	
    	scanf("%d",animales[i].edad);
    	gotoxy(20,4);
    	printf("Datos actualizados!\n");
		
		case 9:
    	gotoxy(20,2);	
    	printf("Ingrese nuevo servicio \n");
    	gotoxy(20,3);	
    	scanf("%s",animales[i].servicio);
    	gotoxy(20,4);
    	printf("Datos actualizados!\n");
		
		default:
    	Admin();
		break;
	}
   }
}
    
    if(cont2==0)
	    {   
			gotoxy(20,5); 	
	    	printf("El Animal que desea modificar no existe.\n");
	    	gotoxy(20,6); 	
	    	printf("\250Intentar de nuevo? 1-Si 2-No \n");
	    	y++;
	    	gotoxy(20,7); 	
    	    scanf("%d",&error);
   	        if(error==1)
   	        {
		       modificar_animal();
	        }
	        else
	        {
    	       Admin();
		    }
	    	
		}
		else
		{
         gotoxy(20,6);
		 printf("\250Desea buscar otro c\xA2""digo ? 1-Si 2-No\n",162); 
		 gotoxy(20,7);	
	     scanf("%d",&op);
        }
	}while(op==1);
	if(op==2)
	{
		Admin();
	}
	else
	{
		Admin();
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
int menu_baja()    // Menu de las bajas

{
	int x=20,y=6;
	
	cuadro();
	gotoxy(30,2);
    printf ("Men\xA3"" Bajas\n");
	gotoxy(30,3);
    printf("---- -----\n\n\n");
	gotoxy(x,y);
	printf("1-Baja due\xA4""o por c\xA2""digo\n");
	y++;
	gotoxy(x,y);
	printf("2-Baja due\xA4""o por apellido\n");
	y++;
	gotoxy(x,y);
	printf("3-Baja animal por c\xA2""digo\n");
	y++;
	gotoxy(x,y);
	printf("4-Baja animal por nombre\n");
	y++;
	gotoxy(x,y);
	printf("5-Volver\n");
	y++;
	gotoxy(x,y);
	scanf("%d",&o);
	y++;
	
	switch(o)
	{
		case 1:
			system("cls");
			baja_duenio_cod();
			break;
		
		case 2:
			system("cls");
			baja_duenio_ap();
			break;
			
		case 3:
			system("cls");
			baja_animal_cod();
			break;
			
			
		case 4:
			system("cls");
			baja_animal_nom();
			break;
			
		case 5:
		    system("cls");
			Admin();
			break;
		
		
	default:
			gotoxy(x,12);
			printf ("Error en el ingreso de opci\xA2""n ");
		    getch ();
		    Admin();
		
			
	}
}
//----------------------------------------------------------------------------------------------------------------------------------
int baja_duenio_cod()     // baja del due�o por codigo

{
	system("cls");   // limpiar pantalla
	int baja,cont,p,er,x=20,y=5;
	char cb[20];
	cuadro();
	gotoxy(20,2);
	printf("Ingrese c\xA2""digo para dar de baja: \n",162);
	gotoxy(20,3);
	scanf("%s",cb);    
	
	for(p=0;p<cd;p++)
	{
		if(!strcmp(persona[p].codigo,cb))
		{
  		 due==p;
  
  		 gotoxy(x,y);
  		 printf("c\xA2""digo: %s \n",persona[p].codigo);
  		 y++;
  		 gotoxy(x,y);
  		 printf("Apellido: %s \n",persona[p].apellido);
  		 y++;
  		 gotoxy(x,y);
		 printf("Nombre: %s \n",persona[p].nombre);
		 y++;
		 gotoxy(x,y);
  		 printf("Tipo de documento: %s\n",persona[p].tipo);
  		 y++;
  		 gotoxy(x,y);
  		 printf("N\xA3mero de documento: %s\n",persona[p].documento);
  		 y++;
  		 gotoxy(x,y);
		 printf("Tel\x82""fono: %s\n",persona[p].telefono);
		 y++;
		 gotoxy(x,y);
		 printf("Direcci\xA2""n: %s\n ",persona[p].direccion); 
		 y++;
  		 cont=1;
     	}
     	
    }
	if(cont==0)
	{
	
		gotoxy(x,y);
		printf("El due\xA4""o que desea dar de baja no existe\n");
		y++;
		gotoxy(x,y);
		printf("\250Intentar otra vez? 1-Si 2-No \n");
		scanf("%d",&er);
		
		switch(er)
		{
		
		
		case 1:
			system("cls");
			baja_duenio_cod();
			break;
			
		default:
			system("cls");
			Admin();
			break;
		}
     }
     else
     {
  
     	gotoxy(x,y);
     	printf("\250Desea dar de baja a este due\xA4""o? 1-Si 2-No\n");
     	y++;
     	gotoxy(x,y);
     	scanf("%d",&baja);
     	if(baja==1)
     	{
     		persona[due].bajlog=1;
     		Admin();
     	}
     	else
     	{
     		Admin();  // vuelve al menu
     	}
     }
     
}			
//----------------------------------------------------------------------------------------------------------------------------------
int baja_duenio_ap()

{
	system("cls");
	int baja,cont,p,er,x=20,y=5;
	char ab[20];
    cuadro();
 	gotoxy(20,2);
	printf("Ingrese apellido para dar de baja: \n");
	gotoxy(20,3);
	scanf("%s",ab);
	
	for(p=0;p<cd;p++)
	{
		if (strcmp(persona[p].apellido,ab)==0)
		{
  		 due=p;
  		
  		 gotoxy(x,y);
  		 printf("c\xA2""digo %s \n",persona[p].codigo);
  		 y++;
  		 gotoxy(x,y);
  		 printf("Apellido: %s \n",persona[p].apellido);
  		 y++;
  		 gotoxy(x,y);
		 printf("Nombre: %s \n",persona[p].nombre);
		 y++;
		 gotoxy(x,y);
  		 printf("Tipo de documento: %s\n",persona[p].tipo);
  		 y++;
  		 gotoxy(x,y);
  		 printf("N\xA3mero de documento: %s\n",persona[p].documento);
  		 y++;
  		 gotoxy(x,y);
		 printf("tel\x82""fono: %s\n",persona[p].telefono);
		 y++;
		 gotoxy(x,y);
		 printf("Direcci\xA2""n: %s\n ",persona[p].direccion); 
		 y++;
		 
  		 cont=1;
     	}
     	
    }
	if(cont==0)
	{
	
		gotoxy(x,y);
		printf("El due\xA4""o que desea dar de baja no existe\n");
		y++;
		gotoxy(x,y);
		printf("\250Intentar otra vez? 1-Si 2-No \n");
		y++;
		gotoxy(x,y);
		scanf("%d",&er);
		
		switch(er)
		{
		
		
		case 1:
			system("cls");
			baja_duenio_ap();
			break;
			
		default:
			system("cls");
			Admin();
			break;
		}
     }
     else
     {
     
     	gotoxy(x,y);
     	printf("\250Desea dar de baja a este due\xA4""o? 1-Si 2-No\n");
     	y++;
     	gotoxy(x,y);
     	scanf("%d",&baja);
     	if(baja==1)
     	{
     		persona[due].bajlog=1;
     	}
     	else
     	{
     		Admin(); // vuelve al menu
     	}
     }
     
}			
//----------------------------------------------------------------------------------------------------------------------------------
int baja_animal_cod()

{
	system("cls");
	int baja,cont,p,er,x=20,y=5;
	char bca[22];
    cuadro();
 	gotoxy(20,2);
	printf("Ingrese c\xA2""digo para dar de baja al animal: \n");
	gotoxy(20,3);
	scanf("%s",bca);
	
	for(p=0;p<cp;p++)
	{
		if (!strcmp(animales[p].cod,bca))
		{
  		 ani=p;
  
  		 gotoxy(x,y);
  		 printf("c\xA2""digo: %s ",animales[p].cod);
  		 y++;
  		 gotoxy(x,y);
  		 printf("Nombre: %s \n",animales[p].nombre);
  		 y++;
  		 gotoxy(x,y);
		 printf("D\xA1""a de Nacimiento: %s \n",animales[p].dia);
		 y++;
		 gotoxy(x,y);
  		 printf("Mes de Nacimiento: %s\n",animales[p].mes);
  		 y++;
  		 gotoxy(x,y);
  		 printf("A\xA4""o de Nacimiento: %d\n",animales[p].anio);
  		 y++;
  		 gotoxy(x,y);
		 printf("Tipo: %s\n",animales[p].tipo);
		 y++;
		 gotoxy(x,y);
		 printf("Raza: %s\n ",animales[p].raza);
		 y++;
		 gotoxy(x,y); 
		 printf("Nombre del due\xA4""o: %s\n",animales[p].duenio);
		 y++;
		 gotoxy(x,y);
		 printf("Edad: %d\n",animales[p].edad);
		 y++;
		 gotoxy(x,y);
		 printf("Servicio: %s\n",animales[p].servicio);
		 y++;
  		 cont=1;
     	}   // cierre del if
     	
    }   // cierre del primer for
	if(cont==0)
	{
	
		y++;
		gotoxy(x,y);
		printf("El animal que desea dar de baja no existe\n");
		y++;
		gotoxy(x,y);
		printf("\250Intentar otra vez? 1-Si 2-No \n");
		y++;
		gotoxy(x,y);
		scanf("%d",&er);
		
		switch(er)
		{
		
		
		case 1:
			system("cls");
			baja_animal_cod();
			break;
			
		default:
			system("cls");
			Admin();
			break;
		}
     }
     else
     {
   
     	gotoxy(x,y);
     	printf("\250Desea dar de baja a este animal? 1-Si 2-No\n");
     	y++;
     	gotoxy(x,y);
     	scanf("%d",&baja);
     	if(baja==1)
     	{
     		animales[ani].bajlog=1;    // da de baja al animal
     	}
     	else
     	{
     		Admin();
     	}
     }
     
}		
//----------------------------------------------------------------------------------------------------------------------------------
int baja_animal_nom()

{
	system("cls");
	int baja,cont,p,er,x=20,y=5;
	char bna[20];
    cuadro();
 	gotoxy(20,2);
	printf("Ingrese nombre del animal para dar de baja: \n");
	gotoxy(20,3);
	scanf("%s",bna);
	
	for(p=0;p<cp;p++)
	{
		if (strcmp(animales[p].nombre,bna)==0)
		{
  		 ani=p;
 
  		 gotoxy(x,y);
  		 printf("C\xA4""digo: %s \n",animales[p].cod,162);
  		 y++;
  		 gotoxy(x,y);
  		 printf("Nombre: %s \n",animales[p].nombre);
  		 y++;
  		 gotoxy(x,y);
		 printf("D\xA1""a de Nacimiento: %s \n",animales[p].dia);
		 y++;
		 gotoxy(x,y);
  		 printf("Mes de Nacimiento: %s\n",animales[p].mes);
  		 y++;
  		 gotoxy(x,y);
  		 printf("A\xA4""o  de Nacimiento: %d\n",animales[p].anio);
  		 y++;
  		 gotoxy(x,y);
		 printf("Tipo: %s\n",animales[p].tipo);
		 y++;
		 gotoxy(x,y);
		 printf("Raza: %s\n ",animales[p].raza);
		 y++;
		 gotoxy(x,y); 
		 printf("Nombre del due\xA4""o: %s\n",animales[p].duenio);
		 y++;
		 gotoxy(x,y);
		 printf("Edad: %d\n",animales[p].edad);
		 y++;
		 gotoxy(x,y);
		 printf("Servicio: %s\n",animales[p].servicio);
		 y++;
		 
  		 cont=1;
     	}
     	
    }
	if(cont==0)
	{
	
		gotoxy(x,y);
		printf("El animal que desea dar de baja no existe\n");
		y++;
		gotoxy(x,y);
		printf("\250Intentar otra vez? 1-Si 2-No \n");
		y++;
		gotoxy(x,y);
		scanf("%d",&er);
		
		switch(er)
		{
		
		
		case 1:
			system("cls");
			baja_animal_nom();
			break;
			
		default:
			system("cls");
			Admin();
			break;
		}
     }
     else
     {
     	gotoxy(x,y);
     	printf("\250Desea dar de baja a este animal? 1-Si 2-No\n");
     	y++;
     	gotoxy(x,y);
     	scanf("%d",&baja);
     	if(baja==1)
     	{
     		animales[ani].bajlog=1;
     	}
     	else
     	{
     		Admin();
     	}
     }
}
int lista_servicio()
{
	int x=0, y=1, f, op;
   gotoxy(0,0);
	printf("��������������������������������������������������������������������������������");
	gotoxy(1,y);
	printf ("c\xA2""digo del animal\n");
	gotoxy(20,y);
	printf("Nombre del animal\n");
	gotoxy(50,y);
	printf("Servicio recibido");
	gotoxy(0,y);
	printf("�");//linea 1 
	gotoxy(19,y);
	printf("�");//linea 2 
	gotoxy(38,y);
	printf("�");//linea 3 
	gotoxy(79,y);
	printf("�");//ultima linea
	y++;
	gotoxy(0,y);
	printf("��������������������������������������������������������������������������������");
	
	for(f=0;f<cp;f++)
	{
		y++;
		gotoxy(1,y);
		printf("%s",animales[f].cod);
		gotoxy(20,y);
		printf("%s",animales[f].nombre);
		gotoxy(39,y);
		printf("%s",animales[f].servicio);
		gotoxy(0,y);
	    printf("�");//linea 1 
	    gotoxy(19,y);
	    printf("�");//linea 2 
	    gotoxy(38,y);
	    printf("�");//linea 3 
		gotoxy(79,y);
	    printf("�");//ultima linea
		
	}
	printf("��������������������������������������������������������������������������������");
	getch();
	
	gotoxy(0,y+1);
	printf("\nPara regresar al men\xA3"" ingrese 1 \n");
	scanf("%d",&op);
	switch(op)
	{
		case  1:
		if(mp==0)
		{
		Admin();
		break;
        }
        else
        {
        Usuario();
        break;
        }
		
		default:
			printf("Error de ingreso");
			if(mp==0)
			{
			getch();
			Admin();
			}
			else
			{
			getch();
			Usuario();
			}
		
			
	}
	getch();

}																						
    							

